package StatsMaker;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class StatObject {
    private boolean isKeyObject;
    private boolean isContour;
    private boolean isDefinition;
    private String objectName;
    private List<Pair <String, Object>> properties;
    private int regionId;
    private double x;
    private double y;

    public StatObject(boolean isKeyObject, boolean isContour, boolean isDefinition, String objectName){
        this.isKeyObject = isKeyObject;
        this.objectName = objectName;
        this.isContour = isContour;
        this.isDefinition = isDefinition;
        properties = new ArrayList<>();
    }

    public void addProperty(String label, Object value){
        properties.add(new Pair<>(label, value));
    }

    public List<Pair<String, Object>> getProperties() {
        return properties;
    }

    public void setX(double x){
        this.x = x;
    }

    public void setY(double y){
        this.y = y;
    }

    public String getObjectName() {
        return objectName;
    }
}