package DataReader;

import StatsMaker.StatObject;
import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class DataReader {
    private File file;  // Zmiana -> implementacyjna = ściezka do pliku - filechooser zwraca w sumie plik,
    // więc nie wiem czy jest sens to zmieniać.

    private List<StatObject> allObjects = new ArrayList<>();
    private List<StatObject> objectsDefinitions = new ArrayList<>();

    public DataReader(File file) {
        this.file = file;
    }


    public void readFile() throws Exception {

        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String line;
        int lastInterpretedComment = -1;
        int lastIndex = -1;
        String[] separated;

        while ((line = bufferedReader.readLine()) != null) {

            if (line.matches("#.*?")) {
                System.out.println("Koment");

                // robimy tutaj rozpoznawanie linii

                separated = line.split(" ");
                lastInterpretedComment = lineInterpreter(separated);

                /*
                 */
            } else {
                separated = line.split(" ");
                if (separated.length != 0) {
                    switch (lastInterpretedComment) {
                        case 0:
                            createNewContourPoint(separated);
                            break;

                        case 1:
                            createNewKeyPoint(separated);
                            break;

                        case 2:
                            createNewObjectDefinition(separated);
                            break;

                        case 3:
                            createNewObject(separated);
                            break;

                        default:
                            throw new Exception("Niepoprawne dane");

                    }
                }
            }
        }

        printOutObjects();
        bufferedReader.close();
    }

    private void printOutObjects() {
       for (StatObject object : allObjects){
           System.out.println(object.getObjectName());

           for (Pair<String, Object> property: object.getProperties()){
               System.out.println("\t" + property.getKey() + " -> " + property.getValue().toString());
           }
       }
    }

    private void createNewObject(String[] separated) {
        if (separated.length > 2){
            StatObject object = new StatObject(false, false, false, separated[1]);

            for (int i = 0; i < objectsDefinitions.size(); i++){
                if (objectsDefinitions.get(i).getObjectName().equals(separated[1])){

                    for (int j = 0; j < objectsDefinitions.get(i).getProperties().size(); j++){
                        Pair<String, Object> property = objectsDefinitions.get(i).getProperties().get(j);

                        switch (property.getValue().toString().toLowerCase()){
                            case "string":
                                String joined = String.join(" ", separated);
                                System.out.println(joined);
                                String[] quoted = joined.split("\"");

                                object.addProperty(property.getKey(), quoted[1]);   //Dla potomnych: biorę quoted[1] bo jest to pierwszy napis zawarty między "..."
                                quoted[1] = ".";
                                separated = String.join("",quoted).split(" ");
                            // narazie tylko dla jedengo string
                                break;

                            case "double":
                                object.addProperty(property.getKey(), Double.valueOf(separated[2+j]));
                                break;

                            case "int":
                                object.addProperty(property.getKey(), Integer.valueOf(separated[2+j]));
                                break;

                        }

                    }
                }
            }
            allObjects.add(object);
        }

    }

    private void createNewObjectDefinition(String[] separated) throws Exception {

        if (separated.length > 3 && separated.length % 2 == 0) {  //Warunek parzystości konieczny, ponieważ mówimy o parach label - value
            StatObject objectDefinition = new StatObject(false, false, true, separated[1]);

            for (int i = 2; i < separated.length; i += 2) {
                objectDefinition.addProperty(separated[i], separated[i + 1]);
            }

            objectsDefinitions.add(objectDefinition);
        } else if (separated.length == 1) {

            //MEGA PROWIZORYCZNY WARUNEK ale działa XD

        } else {
            throw new Exception("Napotkano niepoprawne dane podczas interpretowania definicji obiektów");
        }
    }

    private void createNewKeyPoint(String[] separated) throws Exception {

        if (separated.length > 3) {
            String name = "";
            for (int i = 3; i < separated.length; i++) {
                name += separated[i] + " ";
            }

            StatObject keyPoint = new StatObject(true, false, false, name);
            keyPoint.setX(Integer.valueOf(separated[1]));
            keyPoint.setY(Integer.valueOf(separated[2]));
            allObjects.add(keyPoint);
        } else if (separated.length == 1) {

            //MEGA PROWIZORYCZNY WARUNEK ale działa XD

        } else {
            throw new Exception("Napotkano niepoprawne dane podczas interpretowania punktów kluczowych");
        }
    }

    private void createNewContourPoint(String[] separated) throws Exception {
        if (separated.length == 3) {
            StatObject contourPoint = new StatObject(false, true, false, "ContourPoint" + separated[0]);
            contourPoint.setX(Double.valueOf(separated[1]));
            contourPoint.setY(Double.valueOf(separated[2]));
            allObjects.add(contourPoint);
        } else if (separated.length == 1) {

            //MEGA PROWIZORYCZNY WARUNEK ale działa XD

        } else {
            throw new Exception("Napotkano niepoprawne dane podczas interpretowania konturów terenu");
        }
    }

    private int lineInterpreter(String[] separated) {
        switch (separated[1] + " " + separated[2]) {

            case "Kontury terenu":
                System.out.printf("Następne linie to kontury\n");
                return 0;


            case "Punkty kluczowe:":
                System.out.printf("Następne linie to punkty kluczowe\n");
                return 1;


            case "Definicje obiektów:":
                System.out.printf("Następne linie to definicje obiektów\n");
                return 2;


            case "Obiekty: Typ_obiektu":
                System.out.printf("Następne linie to obiekty\n");
                return 3;


            default:
                System.out.printf("Nie wiem ki chuj wczytałem -> pewnie błąd");
                return -1;

        }

    }

}
