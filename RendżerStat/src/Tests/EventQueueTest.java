package Tests;

import Voronoi.Event;
import Voronoi.EventComparator;
import Voronoi.KeyPointEvent;
import Voronoi.Point;
import org.junit.Test;

import java.util.PriorityQueue;

import static org.junit.Assert.assertEquals;

public class EventQueueTest {

    @Test
    public void addEventLowerY() {
        //GIVEN
        PriorityQueue<Event> events = new PriorityQueue<>(15, new EventComparator());
        events.add(new KeyPointEvent(new Point(14, 56)));

        //WHEN
        events.add(new KeyPointEvent(new Point(12, 13)));

        //THEN
        assertEquals(2, events.size(), 0);
        KeyPointEvent event = (KeyPointEvent) events.peek();
        assert event != null;
        assertEquals(13, event.getYCoordinatePoint(), 0);
    }

    @Test
    public void addEventHigherY() {
        //GIVEN
        PriorityQueue<Event> events = new PriorityQueue<>(15, new EventComparator());
        events.add(new KeyPointEvent(new Point(34, 5)));

        //WHEN
        events.add(new KeyPointEvent(new Point(12, 45)));

        //THEN
        assertEquals(2, events.size(), 0);
        KeyPointEvent event = (KeyPointEvent) events.peek();
        assert event != null;
        assertEquals(5, event.getYCoordinatePoint(), 0);
    }
}
