package Tests;

import Voronoi.Node;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class NodeTest {

    @Test
    public void setParentFromItemNullTest() {
        //GIVEN
        Node element = new Node();
        Node toBeTested = new Node();

        //WHEN
        toBeTested.setParentForItem(element);

        //THEN
        assertNull(toBeTested.getParent());
    }

    @Test
    public void setParentForItemLeftTest() {
        //GIVEN
        Node parent = new Node();
        Node leftChild = new Node(parent, null);
        parent.setLeftChild(leftChild);
        Node element = new Node();

        //WHEN
        element.setParentForItem(leftChild);

        //THEN
        assertEquals(parent, element.getParent());
        assertEquals(element, parent.getLeftChild());
    }

    @Test
    public void setParentFromItemRightTest() {
        //GIVEN
        Node parent = new Node();
        Node rightChild = new Node(parent, null);
        parent.setRightChild(rightChild);
        Node element = new Node();

        //WHEN
        element.setParentForItem(rightChild);

        //THEN
        assertEquals(element, parent.getRightChild());
        assertEquals(parent, element.getParent());
    }
}