package Tests;

import Voronoi.ContourLine;
import Voronoi.Edge;
import Voronoi.Point;
import org.junit.Test;

import static org.junit.Assert.*;

public class ContourLineTest {

    private ContourLine testLine;
    private Edge testEdge;
    private Point commonPoint;

    private void setTestLine(Point startPoint, Point endPoint, Point edgeStart, Point edgeEnd) {
        testLine = new ContourLine(startPoint, endPoint);
        testEdge = new Edge(null, null, edgeStart, new Point(1, 1), new Point(1, 1));
        testEdge.setEndingPoint(edgeEnd);
    }

    private void assertCommonPoint(double expectedX, double expectedY) {
        assertNotNull(commonPoint);
        assertEquals(expectedX, commonPoint.getX(), 0);
        assertEquals(expectedY, commonPoint.getY(), 0);
    }

    @Test
    public void calculateCommonPoint() {
        //GIVEN
        Point edgeStart = new Point(-1, -3), edgeEnd = new Point(1, 3);
        Point start = new Point(2, 2), end = new Point(-2, -2);
        setTestLine(start, end, edgeStart, edgeEnd);

        //WHEN
        commonPoint = testLine.calculateCommonPoint(testEdge);

        //THEN
        assertCommonPoint(0, 0);
    }

    @Test
    public void calculateCommonOnEndingPoint() {
        //GIVEN
        Point start = new Point(7, 9), end = new Point(14, 10);
        Point edgeStart = new Point(6, 12), edgeEnd = new Point(8, 6);
        setTestLine(start, end, edgeStart, edgeEnd);

        //WHEN
        commonPoint = testLine.calculateCommonPoint(testEdge);

        //THEN
        assertCommonPoint(7, 9);
    }

    @Test
    public void noCommonPoint() {
        //GIVEN
        Point start = new Point(3, 5), end = new Point(4, 7);
        Point edgeStart = new Point(5, 5), edgeEnd = new Point(7, 6);
        setTestLine(start, end, edgeStart, edgeEnd);

        //WHEN
        commonPoint = testLine.calculateCommonPoint(testEdge);

        //THEN
        assertNull(commonPoint);
    }

    @Test
    public void theSameXEdge() {
        //GIVEN
        Point start = new Point(1, 11), end = new Point(5, 7);
        Point edgeStart = new Point(4, 6), edgeEnd = new Point(4, 9);
        setTestLine(start, end, edgeStart, edgeEnd);

        //WHEN
        commonPoint = testLine.calculateCommonPoint(testEdge);

        //THEN
        assertCommonPoint(4, 8);
    }

    @Test
    public void parallelLines() {
        //GIVEN
        Point start = new Point(3, 7), end = new Point(5, 5);
        Point edgeStart = new Point(5, 7), edgeEnd = new Point(7, 5);
        setTestLine(start, end, edgeStart, edgeEnd);

        //WHEN
        commonPoint = testLine.calculateCommonPoint(testEdge);

        //THEN
        assertNull(commonPoint);
    }

    @Test
    public void theSameYContour() {
        //GIVEN
        Point start = new Point(5, 8), end = new Point(10, 8);
        Point edgeStart = new Point(7, 6), edgeEnd = new Point(9, 10);
        setTestLine(start, end, edgeStart, edgeEnd);

        //WHEN
        commonPoint = testLine.calculateCommonPoint(testEdge);

        //THEN
        assertCommonPoint(8, 8);
    }

    @Test
    public void theSameYEdgTheSameXContour() {
        //GIVEN
        Point start = new Point(8, 9), end = new Point(12, 9);
        Point edgeStart = new Point(10, 7), edgeEnd = new Point(10, 10);
        setTestLine(start, end, edgeStart, edgeEnd);

        //WHEN
        commonPoint = testLine.calculateCommonPoint(testEdge);

        //THEN
        assertCommonPoint(10, 9);
    }

    @Test
    public void calculateYCoordinate() {
        //GIVEN
        testLine = new ContourLine(new Point(17, 34), new Point(25, 87));

        //WHEN
        double y = testLine.calculateYCoordinate(20);

        //THEN
        assertEquals(53.875, y, 0);
    }

    @Test
    public void calculateYVertical() {
        //GIVEN
        testLine = new ContourLine(new Point(17, 5), new Point(17, 15));

        //WHEN
        double y = testLine.calculateYCoordinate(34);

        //THEN
        assertEquals(17, y, 0);
    }

    @Test
    public void calculateYHorizontal() {
        //GIVEN
        testLine = new ContourLine(new Point(5, 10), new Point(45, 10));

        //WHEN
        double y = testLine.calculateYCoordinate(99);

        //THEN
        assertEquals(10, y, 0);
    }
}