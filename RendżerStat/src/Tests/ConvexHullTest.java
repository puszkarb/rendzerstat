package Tests;

import ConvexHull.ConvexHull;
import Voronoi.Point;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ConvexHullTest {

    private ConvexHull convexHull;
    private ArrayList<Point> points;
    private Random random;

    @Before
    public void setUp() {
        convexHull = new ConvexHull();
        points = new ArrayList<>();
        random = new Random(10);
    }

    @Test
    public void findExistingConvexHullTest() {
        //GIVEN
        setPoints(new double[]{0, 0, 0, 5, 6, 6, 5, 0});

        for (int i = 0; i < 15; i++) {
            points.add(new Point(random.nextDouble() * 5, random.nextDouble() * 5));
        }

        //WHEN
        ArrayList<Point> output = convexHull.FindConvexHull(points);

        //THEN
        assertEquals(4, output.size());
        for (int i = 0; i < 4; i++) {
            assertTrue(output.contains(points.get(i)));
        }

    }

    @Test
    public void findNotExistingConvexHullTest_lessThan3Points() {
        //GIVEN
        setPoints(new double[]{0, 0, 0, 5});

        //WHEN
        ArrayList<Point> output = convexHull.FindConvexHull(points);

        //THEN
        assertEquals(0, output.size());

    }

    @Test
    public void findExistingConvexHull_noPointsGiven() {
        //WHEN
        ArrayList<Point> output = convexHull.FindConvexHull(points);

        //THEN
        assertEquals(0, output.size());

    }

    @Test
    public void findInnerPointsOfTheArea() {
        //GIVEN
        setPoints(new double[]{0, 0, 0, 5, 5, 5, 5, 0});

        for (int i = 0; i < 15; i++) {
            points.add(new Point(random.nextDouble() * 5, random.nextDouble() * 5));
        }

        //WHEN
        ArrayList<Point> output = convexHull.findInnerPointsOfTheArea(points);

        //THEN
        assertEquals(15, output.size());
        for (int i = 0; i < 15; i++) {
            assert (output.contains(points.get(i + 4)));
        }
    }

    @Test
    public void findInnerPointsOfTheArea_noInnerPoints() {
        //GIVEN
        setPoints(new double[]{0, 0, 0, 5, 5, 5, 5, 0});

        //WHEN
        ArrayList<Point> output = convexHull.findInnerPointsOfTheArea(points);

        //THEN
        assertEquals(0, output.size());
    }

    private void setPoints(double[] coordinates) {
        for (int i = 0; i < coordinates.length; i = i + 2) {
            double x = coordinates[i];
            double y = coordinates[i + 1];
            points.add(new Point(x, y));
        }
    }
}
