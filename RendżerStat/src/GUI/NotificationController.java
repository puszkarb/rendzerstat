package GUI;

import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class NotificationController extends Controller {

    private RendzerStat application;
    private Scene scene;

    public void configure(final RendzerStat application, final Scene scene) {
        this.application = application;
        this.scene = scene;
    }

    public void show() {
        application.show(scene);
    }

    @FXML
    private Label notificationLabel;

    void notificationWindow(String message) {

        notificationLabel.setText(message);
        try {
            Scene scene = this.scene;
            Stage stage = new Stage();
            stage.setTitle("Powiadomienie");
            stage.setScene(scene);

            stage.show();
        } catch (Exception e) {
            System.err.println("Błąd przy wyświetlaniu okna powiadomień: " + e.getMessage());
        }
    }
}
