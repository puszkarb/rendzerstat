package GUI;

import ConvexHull.AreaMaker;
import DataReader.Data;
import StatsMaker.Area;
import StatsMaker.StatObject;
import StatsMaker.StatsMaker;
import com.jfoenix.controls.JFXToggleButton;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainStageController extends Controller {

    AreaMaker areaMaker;
    File textInputFile;

    @FXML
    protected Canvas mainStageCanvas;
    @FXML
    protected JFXToggleButton mainToEditorButton;
    @FXML
    protected Canvas mainStageCanvasBackground;
    @FXML
    Label xMainStageLabel;
    @FXML
    Label yMainStageLabel;
    @FXML
    Label nameLabel;
    @FXML
    AnchorPane objectsAnchorPane;
    @FXML
    Tab objectsTab;
    @FXML
    Label areaLabel;
    @FXML
    BarChart<String, Double> barChart;
    @FXML
    GridPane gridScrollPane;
    @FXML
    ScrollPane scrollObjectsPane;
    @FXML
    ScrollPane areaStatisticsScrollPane;

    void setMainToggleButtonActive() {
        mainToEditorButton.setSelected(false);
    }

    public void switchToEditionStage() {
        application.editorStageController.setEditorToggleButtonActive();
        areaMaker = new AreaMaker(application.dataReader, application.statsMaker);
        refreshCanvas(application.editorStageController.editionStageCanvas, false);
        application.editorStageController.refreshObjectChoiceBox();
        application.editorStageController.show();
    }

    public void showOpenFileChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Otwórz Plik");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(".txt", "*.txt"));
        textInputFile = fileChooser.showOpenDialog(new Stage());
        if (textInputFile != null) {
            application.dataReader = new Data();
            application.statsMaker = new StatsMaker();
            setCanvas(mainStageCanvas);
            loadData(textInputFile);
        }
    }

    private void loadData(File file) {
        int errorCode = application.dataReader.readFile(file, false);
        if (errorCode != 0) {
            handleErrorCode(errorCode);
            return;
        }
        areaMaker = new AreaMaker(application.dataReader, application.statsMaker);
        areaMaker.createNewAreas();
        getStatisticsOverAll();
        prepareCheckBoxes(application.dataReader.getObjectsDefinitions(), -1);
        refreshCanvas(mainStageCanvas, false);


    }

    private void setAreaObjectPropertiesStats(Area selectedArea) {
        GridPane localStatisticsPane = new GridPane();
        int row = 0;
        localStatisticsPane.add(new Label("Lokalne statystyki dla wybranego regionu:"), 0, row);
        for (Pair<String, Double> property : selectedArea.getQuantityOfProperties()) {
            localStatisticsPane.add(new Label(property.getKey()), 0, ++row);
            localStatisticsPane.add(new Label(String.valueOf(property.getValue())), 1, row);
        }
        areaStatisticsScrollPane.setContent(localStatisticsPane);
    }

    private void updateStats(int regionID, ArrayList<StatObject> selectedDefinition) {
        barChart.getData().clear();

        int[] quantity = new int[selectedDefinition.size()];
        ArrayList<Integer> quantityArrayList = new ArrayList<>();
        ArrayList<StatObject> existingDefinitions = new ArrayList<>();
        Area area;

        if (regionID > -1) {
            area = areaMaker.getAreasList().get(regionID);
            for (StatObject object : area.getObjects()) {
                for (int i = 0; i < quantity.length; i++) {
                    if (object.getObjectName().equals(selectedDefinition.get(i).getObjectName())) {
                        quantity[i]++;
                    }
                }
            }

            for (int i = 0; i < quantity.length && i < selectedDefinition.size(); i++) {
                if (quantity[i] > 0) {
                    existingDefinitions.add(selectedDefinition.get(i));
                    quantityArrayList.add(quantity[i]);
                }
            }
            setAreaObjectPropertiesStats(area);
        } else {
            for (StatObject object : application.dataReader.getObjects()) {
                for (int i = 0; i < quantity.length; i++) {
                    if (object.getObjectName().equals(selectedDefinition.get(i).getObjectName())) {
                        quantity[i]++;
                    }
                }
            }
            for (int i = 0; i < quantity.length && i < selectedDefinition.size(); i++) {
                if (quantity[i] > 0) {
                    quantityArrayList.add(quantity[i]);
                }
            }
            existingDefinitions = selectedDefinition;
        }


        int row = 0;
        try {
            for (StatObject object : existingDefinitions) {
                int tmp = quantityArrayList.get(row);

                makeANewSeries(object.getObjectName(), (double) tmp);
                row++;
            }
        } catch (Exception ignored) {
        }

        prepareCheckBoxes(existingDefinitions, regionID);

    }

    public void getInfoFromClosestObject() {
        mainStageCanvas.setOnMouseClicked(event -> {
            double distance = Double.MAX_VALUE;
            StatObject chosenObject = null;
            for (StatObject object : application.dataReader.getKeyObjects()) {
                if (Math.sqrt(Math.pow((event.getX() - object.getX()), 2) + Math.pow((event.getY() - object.getY()), 2)) < distance) {
                    chosenObject = object;
                    distance = Math.sqrt(Math.pow((event.getX() - object.getX()), 2) + Math.pow((event.getY() - object.getY()), 2));
                }
            }

            if (chosenObject != null) {
                xMainStageLabel.setText(String.valueOf(chosenObject.getX()));
                yMainStageLabel.setText(String.valueOf(chosenObject.getY()));
                nameLabel.setText(chosenObject.getObjectName());

                for (int i = 0; i < areaMaker.getAreasList().size(); i++) {
                    if (areaMaker.getAreasList().get(i).getKeyPoint() == chosenObject) {
                        areaLabel.setText("Obszar numer: " + i);
                        updateStats(i, application.dataReader.getObjectsDefinitions());
                        break;
                    }
                }

            } else {
                xMainStageLabel.setText("Nie wybrano punktu");
                yMainStageLabel.setText("Nie wybrano punktu");
            }
        });
    }

    public void getStatisticsOverAll() {

        int[] quantity = new int[application.dataReader.getObjectsDefinitions().size()];
        List<Pair<String, Double>> quantityOfProperties = new ArrayList<>();

        for (StatObject object : application.dataReader.getObjects()) {
            for (int i = 0; i < quantity.length; i++) {
                if (object.getObjectName().equals(application.dataReader.getObjectsDefinitions().get(i).getObjectName())) {
                    quantity[i]++;
                }
            }
        }

        for (Area area : areaMaker.getAreasList()) {
            for (Pair<String, Double> propertyQuantity : area.getQuantityOfProperties()) {
                if (quantityOfProperties.isEmpty()) {
                    quantityOfProperties.add(propertyQuantity);
                } else {
                    String name = propertyQuantity.getKey();
                    boolean isElementToBeAdded = true;
                    for (int i = 0; i < quantityOfProperties.size(); i++) {
                        String currentPropertyName = quantityOfProperties.get(i).getKey();
                        if (name.equals(currentPropertyName)) {
                            double newQuantity = quantityOfProperties.get(i).getValue() + propertyQuantity.getValue();
                            quantityOfProperties.set(i, new Pair<>(name, newQuantity));
                            isElementToBeAdded = false;
                        }
                    }
                    if (isElementToBeAdded) {
                        quantityOfProperties.add(new Pair<>(propertyQuantity.getKey(), propertyQuantity.getValue()));
                    }
                }
            }
        }

        int row = 0;
        GridPane gridPane = new GridPane();
        gridPane.add(new Label("Nazwa"), 0, row);
        gridPane.add(new Label("Liczba"), 1, row);

        barChart.getData().clear();

        for (StatObject object : application.dataReader.getObjectsDefinitions()) {
            gridPane.add(new Label(object.getObjectName()), 0, ++row);
            gridPane.add(new Label(String.valueOf(quantity[row - 1])), 1, row);

            int tmp = quantity[row - 1];
            makeANewSeries(object.getObjectName(), (double) tmp);
        }

        for (Pair<String, Double> prop : quantityOfProperties) {
            gridPane.add(new Label(prop.getKey()), 0, ++row);
            gridPane.add(new Label(String.valueOf(prop.getValue())), 1, row);
            makeANewSeries(prop.getKey(), prop.getValue());
        }

        prepareCheckBoxes(application.dataReader.getObjectsDefinitions(), -1);
        barChart.setTitle("Wystąpienia obiektów");
        scrollObjectsPane.setContent(gridPane);
    }

    private void makeANewSeries(String objectName, double quantity) {
        XYChart.Series<String, Double> series = new XYChart.Series<>();
        series.setName(objectName);
        series.getData().add(new XYChart.Data<>("Wystąpienia", quantity));
        barChart.getData().add(series);
    }

    void prepareCheckBoxes(ArrayList<StatObject> definitions, int area) {
        int row = 0;
        int[] selectedDefinitions = new int[application.dataReader.getObjectsDefinitions().size()];
        for (StatObject def : definitions) {
            for (int i = 0; i < application.dataReader.getObjectsDefinitions().size(); i++) {
                if (def.equals(application.dataReader.getObjectsDefinitions().get(i))) {
                    selectedDefinitions[i]++;
                }
            }
        }
        gridScrollPane.getChildren().clear();
        gridScrollPane.getColumnConstraints().clear();
        gridScrollPane.getRowConstraints().clear();
        gridScrollPane.setVgap(15);

        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(50);
        ColumnConstraints column2 = new ColumnConstraints();
        column1.setPercentWidth(50);

        gridScrollPane.getColumnConstraints().addAll(column1, column2);
        gridScrollPane.add(new Label("Obiekty"), 0, row);
        gridScrollPane.add(new Label("Wybór"), 1, row);

        gridScrollPane.getColumnConstraints().get(1).setPercentWidth(50);


        for (StatObject definition : application.dataReader.getObjectsDefinitions()) {
            row++;
            gridScrollPane.add(new Label(definition.getObjectName()), 0, row);
            CheckBox checkBox = new CheckBox();
            checkBox.setId(definition.getObjectName());
            if (selectedDefinitions[row - 1] > 0) {
                checkBox.setSelected(true);
            } else {
                checkBox.setSelected(false);
            }
            checkBox.setOnMouseClicked((MouseEvent event) -> {
                ArrayList<String> checkBoxesSelected = new ArrayList<>();
                for (Node node : gridScrollPane.getChildren()) {
                    if (node.getClass() == CheckBox.class) {
                        if (((CheckBox) node).isSelected()) {
                            checkBoxesSelected.add(node.getId());
                        }
                    }
                }
                ArrayList<StatObject> selected = new ArrayList<>();
                for (String name : checkBoxesSelected) {
                    for (int i = 0; i < application.dataReader.getObjectsDefinitions().size(); i++) {
                        if (name.equals(application.dataReader.getObjectsDefinitions().get(i).getObjectName())) {
                            selected.add(application.dataReader.getObjectsDefinitions().get(i));
                            break;
                        }
                    }
                }
                updateStats(area, selected);
            });
            gridScrollPane.add(checkBox, 1, row);
        }
    }
}
