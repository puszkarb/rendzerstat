package GUI;

import StatsMaker.StatObject;
import Voronoi.ContourLine;
import Voronoi.Edge;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Controller {

    RendzerStat application;
    protected Scene scene;
    Editor editor = Editor.getInstance();

    @FXML
    protected ImageView saveImageView;
    @FXML
    protected ImageView loadImageView;
    @FXML
    protected ImageView rendzerImageView;

    public void configure(final RendzerStat application, final Scene scene) {
        this.application = application;
        this.scene = scene;
    }

    public void show() {
        application.show(scene);
    }

    void toMainStage() {
        setCanvas(application.mainStageController.mainStageCanvas);
        application.mainStageController.show();
    }

    void setCanvas(Canvas canvas) {
        canvas.getGraphicsContext2D().clearRect(0, 0, 800, 750);
    }

    void refreshCanvas(Canvas canvas, boolean returnFromEditionStage) {
        application.dataReader.checkContours();
        setCanvas(canvas);
        List<StatObject> contours = application.dataReader.getContours(),
                objects = application.dataReader.getObjects();
        if (contours.size() == 0 && objects.size() > 0) {
            handleErrorCode(777);
            return;
        }
        if (!returnFromEditionStage && checkIfOutContour(objects)) {
            return;
        }
        List<StatObject> keyObjects = application.dataReader.getKeyObjects();
        for (StatObject object : editor.getObjects()) {
            System.out.println(object.getObjectName());
        }
        System.out.println("Koniec wyników");
        paintPointsOnCanvas(contours, canvas);
        paintPointsOnCanvas(objects, canvas);
        paintPointsOnCanvas(keyObjects, canvas);

        if (returnFromEditionStage) {
            application.statsMaker.setFinishedEdges(new ArrayList<>());
            prepareDiagramAndPaintEdges();
        } else if (canvas == application.mainStageController.mainStageCanvas && application.statsMaker.getFinishedEdges().size() != 0 && keyObjects.size() > 0) {
            application.statsMaker.setFinishedEdges(new ArrayList<>());
            application.statsMaker.setKeyPoints(new ArrayList<>());
            application.statsMaker.addNewKeyPoint(application.dataReader.getKeyObjects());
            prepareDiagramAndPaintEdges();
        } else if (canvas == application.mainStageController.mainStageCanvas && application.statsMaker.getFinishedEdges().size() == 0
                && keyObjects.size() > 0) {
            application.statsMaker.addNewKeyPoint(application.dataReader.getKeyObjects());
            prepareDiagramAndPaintEdges();
        }
    }

    boolean checkIfOutContour(List<StatObject> objects) {
        List<ContourLine> contours = application.dataReader.getContourLines();
        if (contours == null) {
            return false;
        }
        for (int i = 0; i < objects.size(); i++) {
            StatObject ob = objects.get(i);
            int numberOfCrossing = Edge.getNumberOfCrossing(contours, ob.getX(), ob.getY());
            if (numberOfCrossing % 2 == 0) {
                Alert questionAlert = new Alert(Alert.AlertType.CONFIRMATION);
                questionAlert.setTitle("Punkt poza konturem");
                if (ob.isKeyObject()) {
                    questionAlert.setHeaderText("Punkt kluczowy jest poza konturem!\nCzy kontynuować działanie programu bez tego punktu?");
                    application.dataReader.removeKeyObjectFromKeyObjectList(ob);
                } else if (!ob.isKeyObject() && ob.isNotDefinition() && !ob.isContour()) {
                    questionAlert.setHeaderText("Obiekt jest poza konturem!\nCzy kontynuować działanie programu bez tego puntku?");
                }
                questionAlert.setContentText("Współrzędne punktu: x = " + ob.getX() + " y = " + ob.getY());
                Optional<ButtonType> result = questionAlert.showAndWait();
                if (result.isPresent() && result.get() == ButtonType.OK) {
                    objects.remove(ob);
                    i--;
                } else {
                    return true;
                }
            }
        }
        return false;
    }

    private void prepareDiagramAndPaintEdges() {
        application.statsMaker.prepareDiagram(application.dataReader.getContourLines());
        List<Edge> finishedEdges = application.statsMaker.getFinishedEdges();
        paintFinishedEdges(finishedEdges, application.mainStageController.mainStageCanvas);
    }

    public void showSaveFileChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Zapisz Plik");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(".txt", ".txt"));
        File file = fileChooser.showSaveDialog(new Stage());
        if (file != null) {
            try {
                application.dataReader.save(file);
            } catch (IOException e) {
                application.notificationStageController.notificationWindow("Nie udało się zapisać pliku.");
            }
        }
    }

    private void paintFinishedEdges(List<Edge> finishedEdges, Canvas canvas) {
        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        graphicsContext.setLineWidth(1);
        for (Edge e : finishedEdges) {
            graphicsContext.setStroke(Color.BLACK);
            graphicsContext.strokeLine(e.getStartingPoint().getX(), e.getStartingPoint().getY(), e.getEndingPoint().getX(), e.getEndingPoint().getY());
        }
    }

    private void paintPointsOnCanvas(List<StatObject> objects, Canvas canvas) {
        double diameter;
        Color color;
        if (objects == application.dataReader.getKeyObjects()) {
            diameter = 10;
            color = Color.RED;
        } else if (objects == application.dataReader.getObjects()) {
            diameter = 5;
            color = Color.GREEN;
        } else {
            diameter = 7.5;
            color = Color.PURPLE;
            application.dataReader.checkContours();
            ArrayList<StatObject> contours = application.dataReader.getContours();
            for (int i = 1; i < contours.size(); i++) {
                canvas.getGraphicsContext2D().strokeLine(contours.get(i - 1).getX(),
                        contours.get(i - 1).getY(),
                        contours.get(i).getX(),
                        contours.get(i).getY());
            }
            if (contours.size() > 0) {
                canvas.getGraphicsContext2D().strokeLine(contours.get(contours.size() - 1).getX(), contours.get(contours.size() - 1).getY(), contours.get(0).getX(), contours.get(0).getY());
            }
        }

        canvas.getGraphicsContext2D().setFill(color);
        for (StatObject object : objects) {
            double x = 0, y = 0;
            if (object.getX() >= diameter) {
                x = object.getX() - (diameter / 2);
            }
            if (object.getY() >= diameter) {
                y = object.getY() - diameter / 2;
            }
            canvas.getGraphicsContext2D().fillOval(x, y, diameter, diameter);
        }
    }

    void handleErrorCode(int errorCode) {
        Alert error = new Alert(Alert.AlertType.ERROR);
        error.setTitle("Błąd!");
        int errorLine = application.dataReader.getErrorLine();
        switch (errorCode) {
            case 666:
                error.setHeaderText("Podany plik jest nieprawidłowy!");
                break;
            case 1:
                error.setHeaderText("Podana definicja punktu kluczowego jest nieprawidłowa!");
                break;
            case 2:
                error.setHeaderText("Podana definicja punktu konturowego jest nieprawidłowa!");
                break;
            case 3:
                error.setHeaderText("Podana definicja obiektu jest nieprawidłowa!");
                break;
            case 7:
                error.setHeaderText("Podana deklaracja obiektu jest nieprawidłowa!");
                break;
            case 345:
                error.setHeaderText("Podano nieprawidłowy napis w polu o typie String.\nPowinien zaczynać się od \"");
                break;
            case 356:
                error.setHeaderText("Odwołano się do nieistniejacego typu obiektu!");
                break;
            case 420:
                error.setHeaderText("Podany punkt znajduje się poza planszą!");
                break;
            case 777:
                error.setHeaderText("Nie podano konturu!");
                break;
            case 888:
                error.setHeaderText("Podano nieprawdiłową liczbę we współrzędnych punktu!");
                break;
            case 966:
                error = new Alert(Alert.AlertType.CONFIRMATION);
                error.setTitle("Potwierdź wczytywanie");
                error.setHeaderText("Podany punkt kluczowy już istnieje!\nCzy kontynuować pracę bez duplikatu?");
                error.setContentText("Duplikat znajduje się w linii nr " + errorLine);
                Optional<ButtonType> result = error.showAndWait();
                if (result.isPresent() && result.get() == ButtonType.OK) {
                    application.dataReader.readFile(application.mainStageController.textInputFile, true);
                    refreshCanvas(application.mainStageController.mainStageCanvas, false);
                }
                break;
            case 997:
                error.setHeaderText("Podane dane w pliku są niezgodne z wymaganymi!");
                break;
        }

        if (errorCode != 666 && errorCode != 966 & errorCode != 777) {
            error.setContentText("Numer linii w pliku tekstowym: " + errorLine);
        }
        if (errorCode != 966) {
            error.showAndWait();
        }
    }

    EventHandler<MouseEvent> doNothing() {
        return event -> {
            //do nothing;
        };
    }

}
