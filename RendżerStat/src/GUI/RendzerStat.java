package GUI;

import DataReader.Data;
import StatsMaker.StatsMaker;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

public class RendzerStat extends Application {

    StatsMaker statsMaker;
    EditorController editorStageController;
    MainStageController mainStageController;
    NotificationController notificationStageController;
    private Stage primaryStage;
    Data dataReader;
    private Image helmet = new Image("/pink_ranger_helmet.png");
    private Image pinkRanger = new Image("/pink_ranger_2.png");

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(final Stage primaryStage) throws Exception {
        dataReader = new Data();
        statsMaker = new StatsMaker();
        this.primaryStage = primaryStage;
        mainStageController = loadMainStage();
        editorStageController = loadEditionStage();
        notificationStageController = loadNotification();
        this.primaryStage.setTitle("RendzerStat");
        configureImages();
        mainStageController.toMainStage();
        primaryStage.show();
    }

    private EditorController loadEditionStage() throws IOException {
        final FXMLLoader loader = new FXMLLoader(getClass().getResource("/resources/gui_koncept_edycja.fxml"));
        loader.load();
        final EditorController controller = loader.getController();
        controller.configure(this, new Scene(loader.getRoot()));
        return controller;
    }

    private MainStageController loadMainStage() throws IOException {
        final FXMLLoader loader = new FXMLLoader(getClass().getResource("/resources/gui_koncept.fxml"));
        loader.load();
        final MainStageController controller = loader.getController();
        controller.configure(this, new Scene(loader.getRoot()));
        return controller;
    }

    private NotificationController loadNotification() throws IOException {
        final FXMLLoader loader = new FXMLLoader(getClass().getResource("/notification.fxml"));
        loader.load();
        final NotificationController controller = loader.getController();
        controller.configure(this, new Scene(loader.getRoot()));
        return controller;
    }

    private void configureImages() {
        editorStageController.saveImageView.setImage(helmet);
        editorStageController.rendzerImageView.setImage(pinkRanger);
        mainStageController.saveImageView.setImage(new Image("/pink_ranger_helmet.png"));
        mainStageController.loadImageView.setImage(new Image("/pink_ranger_helmet.png"));
        mainStageController.rendzerImageView.setImage(new Image("/pink_ranger_2.png"));
    }

    void show(final Scene scene) {
        primaryStage.setScene(scene);
    }
}
