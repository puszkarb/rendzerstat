package Voronoi;

public class EdgeIntersectionEvent implements Event {

    private Arc squeezedArc;
    private Point intersectionPoint;

    EdgeIntersectionEvent(Point intersectionPoint, Arc squeezedArc) {
        this.intersectionPoint = intersectionPoint;
        this.squeezedArc = squeezedArc;
    }

    Arc getSqueezedArc() {
        return squeezedArc;
    }

    Point getIntersectionPoint() {
        return intersectionPoint;
    }

    @Override
    public double getYCoordinatePoint() {
        return intersectionPoint.getY();
    }

    @Override
    public double getXCoordinatePoint() {
        return intersectionPoint.getX();
    }

    @Override
    public Point getPoint() {
        return intersectionPoint;
    }
}
