package Voronoi;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class CoastlineTree {

    private Node root;
    private Node none;

    public CoastlineTree(KeyPointEvent event) {
        none = new Node();
        root = new Arc(null, none, event.getPoint());
    }

    public void endEdges(Node current, List<Edge> finishedEdges) {
        if (!current.isEdge()) {
            return;
        }
        double x = getXFromEdge((Edge) current, 1550);

        if (Math.abs(x - ((Edge) current).getMiddlePoint().getX()) < 1) {
            ((Edge) current).setEndingPoint(new Point(x, 10000.0));
        } else {
            ((Edge) current).setEndingPoint(new Point(x, ((Edge) current).getSlope() * x + ((Edge) current).getyCoordinate()));
        }
        finishedEdges.add((Edge) current);
        endEdges(current.getLeftChild(), finishedEdges);
        endEdges(current.getRightChild(), finishedEdges);
    }

    private double findYForEdge(Point oldArcPoint, Point newPoint) {
        double dp = 2 * (oldArcPoint.getY() - newPoint.getY());
        if (dp == 0) {
            return -100000000.0;
        }
        double aCoefficient = 1 / dp;
        double bCoefficient = -2 * oldArcPoint.getX() / dp;
        double cCoefficient = (Math.pow(oldArcPoint.getX(), 2) + Math.pow(oldArcPoint.getY(), 2) - Math.pow(newPoint.getY(), 2)) / dp;
        return (aCoefficient * newPoint.getX() * newPoint.getX() + bCoefficient * newPoint.getX() + cCoefficient);
    }

    private int calculateCircle(Point a, Point b, Point c) {
        double area = (b.getX() - a.getX()) * (c.getY() - a.getY()) - (b.getY() - a.getY()) * (c.getX() - a.getX());
        if (area > 0) {
            return 1;
        } else if (area < 0) {
            return -1;
        } else {
            return 0;
        }
    }

    private void calculateNewEdgeIntersectionEvent(Arc arcToBeSqueezed, PriorityQueue<Event> eventQueue, double sweepLineY) {
        Edge leftEdge = this.getFirstParentOnTheLeft(arcToBeSqueezed);
        Edge rightEdge = this.getFirstParentOnTheRight(arcToBeSqueezed);

        if (leftEdge == null || rightEdge == null) {
            return;
        }

        Arc leftChild = getFirstChildOnTheLeft(leftEdge);
        Arc rightChild = getFirstChildOnTheRight(rightEdge);

        if (leftChild == null || rightChild == null || leftChild.getFocusPoint() == rightChild.getFocusPoint()) {
            return;
        }
        if (calculateCircle(leftChild.getFocusPoint(), arcToBeSqueezed.getFocusPoint(), rightChild.getFocusPoint()) != 1) {
            return;
        }

        Point intersectionPoint = leftEdge.doesIntersectWith(rightEdge);
        if (intersectionPoint == null) {
            return;
        }

        double dx = arcToBeSqueezed.getFirstPointX() - intersectionPoint.getX();
        double dy = arcToBeSqueezed.getFirstPointY() - intersectionPoint.getY();
        double length = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
        if (intersectionPoint.getY() + length < sweepLineY) {
            return;
        }

        Point ep = new Point(intersectionPoint.getX(), intersectionPoint.getY() + length);
        EdgeIntersectionEvent newEvent = new EdgeIntersectionEvent(ep, arcToBeSqueezed);
        arcToBeSqueezed.setSqueezeEvent(newEvent);
        eventQueue.add(newEvent);
    }

    private double[] calculateABC(Point x, double sweepLine) {
        double d = 2 * (x.getY() - sweepLine);
        double a, b, c;
        a = 1 / d;
        b = -2 * x.getX() / d;
        c = (x.getX() * x.getX() + x.getY() * x.getY() - sweepLine * sweepLine) / d;
        return new double[]{a, b, c};
    }

    private double getXFromEdge(Edge edge, double sweepLineY) {
        Arc leftChild = this.getFirstChildOnTheLeft(edge);
        Arc rightChild = this.getFirstChildOnTheRight(edge);
        assert leftChild != null && rightChild != null;
        Point leftChildPoint = leftChild.getFocusPoint();
        Point rightChildPoint = rightChild.getFocusPoint();

        double aLeftPoint, bLeftPoint, cLeftPoint, aRightPoint, bRightPoint, cRightPoint;
        double[] leftChildPointABC = calculateABC(leftChildPoint, sweepLineY);
        aLeftPoint = leftChildPointABC[0];
        bLeftPoint = leftChildPointABC[1];
        cLeftPoint = leftChildPointABC[2];
        double[] rightChildPointABC = calculateABC(rightChildPoint, sweepLineY);
        aRightPoint = rightChildPointABC[0];
        bRightPoint = rightChildPointABC[1];
        cRightPoint = rightChildPointABC[2];

        double aCoefficient = aLeftPoint - aRightPoint;
        double bCoefficient = bLeftPoint - bRightPoint;
        double cCoefficient = cLeftPoint - cRightPoint;

        if (aCoefficient == 0) {
            return -cCoefficient / bCoefficient;
        }

        double disc = bCoefficient * bCoefficient - 4 * aCoefficient * cCoefficient;
        double x1 = (-bCoefficient + Math.sqrt(disc)) / (2 * aCoefficient);
        double x2 = (-bCoefficient - Math.sqrt(disc)) / (2 * aCoefficient);

        return leftChildPoint.getY() > rightChildPoint.getY() ? Math.max(x1, x2) : Math.min(x1, x2);
    }

    private Arc getArcByXCoordinate(Point keyPoint) {
        Node treeNode = root;
        double x, xCoordinate = keyPoint.getX();
        while (treeNode.isEdge()) {
            x = getXFromEdge((Edge) treeNode, keyPoint.getY());
            if (x > xCoordinate) {
                treeNode = treeNode.getLeftChild();
            } else {
                treeNode = treeNode.getRightChild();
            }
        }
        assert treeNode instanceof Arc;
        return (Arc) treeNode;
    }

    public void addArc(KeyPointEvent newPointEvent, PriorityQueue<Event> eventQueue) {
        final Point keyPoint = newPointEvent.getPoint();
        Arc higherArc = getArcByXCoordinate(keyPoint);
        if (higherArc.getSqueezeEvent() != null) {
            eventQueue.remove(higherArc.getSqueezeEvent());
            higherArc.setSqueezeEvent(null);
        }
        Point edgeStartPoint = new Point(keyPoint.getX(), findYForEdge(higherArc.getFocusPoint(), keyPoint));
        Edge leftEdge = new Edge(higherArc.getParent(), none, edgeStartPoint, higherArc.getFocusPoint(), keyPoint);
        Edge rightEdge = new Edge(leftEdge, none, new Point(edgeStartPoint), keyPoint, higherArc.getFocusPoint());
        Arc leftOldArc = new Arc(leftEdge, none, higherArc.getFocusPoint());
        Arc middleNewArc = new Arc(rightEdge, none, keyPoint);
        Arc rightOldArc = new Arc(rightEdge, none, higherArc.getFocusPoint());
        setOrder(higherArc, leftEdge);
        leftEdge.setLeftChild(leftOldArc);
        leftEdge.setRightChild(rightEdge);
        rightEdge.setLeftChild(middleNewArc);
        rightEdge.setRightChild(rightOldArc);

        calculateNewEdgeIntersectionEvent(leftOldArc, eventQueue, keyPoint.getY());
        calculateNewEdgeIntersectionEvent(rightOldArc, eventQueue, keyPoint.getY());
    }

    public void deleteArc(PriorityQueue<Event> eventQueue, EdgeIntersectionEvent intersectionEvent, List<Edge> endedEdges) {
        Arc squeezedArc = intersectionEvent.getSqueezedArc();
        Edge leftEdge = getFirstParentOnTheLeft(squeezedArc);
        Edge rightEdge = getFirstParentOnTheRight(squeezedArc);
        Arc leftArc = getFirstChildOnTheLeft(leftEdge);
        Arc rightArc = getFirstChildOnTheRight(rightEdge);

        assert leftArc != null && rightArc != null;
        if (leftArc.getSqueezeEvent() != null) {
            eventQueue.remove(leftArc.getSqueezeEvent());
            leftArc.setSqueezeEvent(null);
        }
        if (rightArc.getSqueezeEvent() != null) {
            eventQueue.remove(rightArc.getSqueezeEvent());
            rightArc.setSqueezeEvent(null);
        }

        Point endingPoint = new Point(intersectionEvent.getIntersectionPoint().getX(), findYForEdge(squeezedArc.getFocusPoint(), intersectionEvent.getIntersectionPoint()));
        leftEdge.setEndingPoint(endingPoint);
        rightEdge.setEndingPoint(new Point(endingPoint));
        endedEdges.add(leftEdge);
        endedEdges.add(rightEdge);

        Node higherNode = null;
        Node tempNode = squeezedArc;
        while (tempNode.getParent() != null) {
            tempNode = tempNode.getParent();
            if (tempNode == leftEdge) {
                higherNode = leftEdge;
            }
            if (tempNode == rightEdge) {
                higherNode = rightEdge;
            }
        }
        assert higherNode != null;
        Edge higherEdge = new Edge(higherNode.getParent(), none, new Point(endingPoint), leftArc.getFocusPoint(), rightArc.getFocusPoint());
        higherEdge.setLeftChild(higherNode.getLeftChild());
        higherNode.getLeftChild().setParent(higherEdge);
        higherEdge.setRightChild(higherNode.getRightChild());
        higherNode.getRightChild().setParent(higherEdge);
        setOrder(higherNode, higherEdge);
        Edge grandParent = (Edge) squeezedArc.getParent().getParent();
        if (squeezedArc.getParent().getLeftChild() == squeezedArc) {
            if (grandParent.getLeftChild() == squeezedArc.getParent()) {
                grandParent.setLeftChild(squeezedArc.getParent().getRightChild());
            } else {
                grandParent.setRightChild(squeezedArc.getParent().getRightChild());
            }
        } else {
            if (grandParent.getLeftChild() == squeezedArc.getParent()) {
                grandParent.setLeftChild(squeezedArc.getParent().getLeftChild());
            } else {
                grandParent.setRightChild(squeezedArc.getParent().getLeftChild());
            }
        }

        calculateNewEdgeIntersectionEvent(leftArc, eventQueue, intersectionEvent.getYCoordinatePoint());
        calculateNewEdgeIntersectionEvent(rightArc, eventQueue, intersectionEvent.getYCoordinatePoint());
    }

    private void setOrder(Node higherNode, Edge higherEdge) {
        if (root == higherNode) {
            root = higherEdge;
        } else {
            if (higherNode.getParent().getLeftChild() == higherNode) {
                higherNode.getParent().setLeftChild(higherEdge);
            } else {
                higherNode.getParent().setRightChild(higherEdge);
            }
        }
    }

    private Edge getFirstParentOnTheLeft(Arc child) {
        Node currentNode = child;
        while ((currentNode.getParent() != null) && (currentNode.getParent().getLeftChild() == currentNode)) {
            currentNode = currentNode.getParent();
        }
        assert ((currentNode.getParent() == null) || (currentNode.getParent() instanceof Edge));
        return (Edge) currentNode.getParent();
    }

    private Edge getFirstParentOnTheRight(Arc child) {
        Node currentNode = child;
        while ((currentNode.getParent() != null) && (currentNode.getParent().getRightChild() == currentNode)) {
            currentNode = currentNode.getParent();
        }
        assert ((currentNode.getParent() == null) || (currentNode.getParent() instanceof Edge));
        return (Edge) currentNode.getParent();
    }

    private Arc getFirstChildOnTheLeft(Edge parent) {
        if (parent.getLeftChild() == none) {
            return null;
        }
        Node tempNode = parent.getLeftChild();
        while (tempNode.isEdge()) {
            tempNode = tempNode.getRightChild();
        }
        assert tempNode instanceof Arc;
        return (Arc) tempNode;
    }

    private Arc getFirstChildOnTheRight(Edge parent) {
        if (parent.getRightChild() == none) {
            return null;
        }
        Node tempNode = parent.getRightChild();
        while (tempNode.isEdge()) {
            tempNode = tempNode.getLeftChild();
        }
        assert tempNode instanceof Arc;
        return (Arc) tempNode;
    }

    public Node getRoot() {
        return root;
    }

    private List<Node> nodes = new ArrayList<>();

    public void printTreeInOrder(Node node) {
        if (node == none) {
            return;
        } else {
            nodes.add(node);
        }
        printTreeInOrder(node.getLeftChild());
        System.out.println(node.getFirstPointX() + " " + node.getFirstPointY());
        printTreeInOrder(node.getRightChild());
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public void specialEvent(KeyPointEvent event) {
        Point keyPoint = event.getPoint();
        Arc newArc = new Arc(null, none, keyPoint);
        Arc activeArc = getArcByXCoordinate(keyPoint);
        Point edgeStart = new Point((keyPoint.getX() + activeArc.getFocusPoint().getX()) / 2, keyPoint.getY());
        Edge newEdge = new Edge(activeArc.getParent(), none, edgeStart, new Point(keyPoint), new Point(activeArc.getFocusPoint()));
        newEdge.getStartingPoint().setY(0);
        if (activeArc.getParent() != null) {
            if (activeArc.getParent().getLeftChild() == activeArc) {
                activeArc.getParent().setLeftChild(newEdge);
            } else {
                activeArc.getParent().setRightChild(newEdge);
            }
        } else {
            root = newEdge;
        }
        if (keyPoint.getX() < activeArc.getFocusPoint().getX()) {
            newEdge.setLeftChild(newArc);
            newEdge.setRightChild(activeArc);
        } else {
            newEdge.setLeftChild(activeArc);
            newEdge.setRightChild(newArc);
        }
    }
}
