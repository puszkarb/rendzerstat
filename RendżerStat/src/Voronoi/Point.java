package Voronoi;

public class Point {

    private double x;
    private double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point() {
    }

    public Point(Point toBeCopied) {
        x = toBeCopied.getX();
        y = toBeCopied.getY();
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public int hashCode() {
        return (int)(509 * Math.round(x) + 701 * Math.round(y));
    }
}
