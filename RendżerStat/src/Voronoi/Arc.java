package Voronoi;

public class Arc extends Node {

    final private Point focusPoint;
    private EdgeIntersectionEvent squeezeEvent;

    Arc(Node parent, Node none, Point focusPoint) {
        super(parent, none);
        this.focusPoint = focusPoint;
    }

    void setSqueezeEvent(EdgeIntersectionEvent squeezeEvent) {
        this.squeezeEvent = squeezeEvent;
    }

    EdgeIntersectionEvent getSqueezeEvent() {
        return squeezeEvent;
    }

    Point getFocusPoint() {
        return focusPoint;
    }

    @Override
    public double getFirstPointX() {
        return focusPoint.getX();
    }

    @Override
    public double getFirstPointY() {
        return focusPoint.getY();
    }

    @Override
    public boolean isEdge() {
        return false;
    }

}
