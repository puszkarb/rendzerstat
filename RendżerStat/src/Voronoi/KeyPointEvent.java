package Voronoi;

public class KeyPointEvent implements Event {

    private Point newKeyPoint;

    public KeyPointEvent(Point newKeyPoint) {
        this.newKeyPoint = newKeyPoint;
    }

    @Override
    public double getYCoordinatePoint() {
        return newKeyPoint.getY();
    }

    @Override
    public double getXCoordinatePoint() {
        return newKeyPoint.getX();
    }

    @Override
    public Point getPoint() {
        return newKeyPoint;
    }
}
