package Voronoi;

public class Node {

    private Node parent;
    private Node leftChild;
    private Node rightChild;

    public Node() {
        parent = leftChild = rightChild = null;
    }

    public Node(Node parent, Node none) {
        this.parent = parent;
        leftChild = rightChild = none;
    }

    public void setParentForItem(Node item) {
        if (item.getParent() == null) {
            this.setParent(null);
            return;
        }
        if (item.getParent().getLeftChild() == item) {
            item.getParent().setLeftChild(this);
        } else if (item.getParent().getRightChild() == item) {
            item.getParent().setRightChild(this);
        }
        this.setParent(item.getParent());
    }

    public Node getParent() {
        return parent;
    }

    void setParent(Node parent) {
        this.parent = parent;
    }

    public void setLeftChild(Node leftChild) {
        this.leftChild = leftChild;
        leftChild.setParent(this);
    }

    public void setRightChild(Node rightChild) {
        this.rightChild = rightChild;
        rightChild.setParent(this);
    }

    public Node getLeftChild() {
        return leftChild;
    }

    public Node getRightChild() {
        return rightChild;
    }

    public double getFirstPointY() {
        return 0;
    }

    public double getFirstPointX() {
        return 0;
    }

    public boolean isEdge() {
        return false;
    }
}
