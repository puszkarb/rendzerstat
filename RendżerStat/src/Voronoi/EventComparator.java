package Voronoi;

import java.util.Comparator;

public class EventComparator implements Comparator<Event> {

    @Override
    public int compare(Event o1, Event o2) {
        int comparison = Double.compare(o1.getYCoordinatePoint(), o2.getYCoordinatePoint());

        if (comparison > 0) {
            return 1;
        } else if (comparison < 0) {
            return -1;
        } else {
            comparison = Double.compare(o1.getXCoordinatePoint(), o2.getXCoordinatePoint());
            if (comparison > 0) {
                return 1;
            } else if (comparison < 0) {
                return -1;
            } else {
                return 0;
            }
        }
    }
}
