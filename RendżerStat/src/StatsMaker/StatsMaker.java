package StatsMaker;

import Voronoi.*;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class StatsMaker {

    private PriorityQueue<Event> eventQueue;
    private List<Point> keyPoints;
    private List<Edge> finishedEdges;

    public StatsMaker() {
        eventQueue = new PriorityQueue<>(15, new EventComparator());
        keyPoints = new ArrayList<>();
        finishedEdges = new ArrayList<>();
    }

    public StatsMaker(List<Point> keyPoints) {
        eventQueue = new PriorityQueue<>(15, new EventComparator());
        this.keyPoints = keyPoints;
        finishedEdges = new ArrayList<>();
    }

    public void prepareDiagram(List<ContourLine> contoursLines) {
        for (Point p : keyPoints) {
            KeyPointEvent newPointEvent = new KeyPointEvent(p);
            eventQueue.add(newPointEvent);
        }
        CoastlineTree coastlineTree = new CoastlineTree((KeyPointEvent) eventQueue.remove());

        Event event;
        while (!eventQueue.isEmpty() && eventQueue.peek().getYCoordinatePoint() < coastlineTree.getRoot().getFirstPointY() + 1 && coastlineTree.getRoot() instanceof Arc) {
            event = eventQueue.remove();
            assert event instanceof KeyPointEvent;
            coastlineTree.specialEvent((KeyPointEvent) event);
        }

        while (!eventQueue.isEmpty()) {
            event = eventQueue.remove();
            if (event.getClass() == KeyPointEvent.class) {
                coastlineTree.addArc((KeyPointEvent) event, eventQueue);
            } else if (event.getClass() == EdgeIntersectionEvent.class) {
                coastlineTree.deleteArc(eventQueue, (EdgeIntersectionEvent) event, finishedEdges);
            }
        }

        coastlineTree.endEdges(coastlineTree.getRoot(), finishedEdges);
        for (int i = 0; i < finishedEdges.size(); i++) {
            Edge e = finishedEdges.get(i);
            e.correctWithinBorders(contoursLines);
            if (e.getStartingPoint().getX() == e.getEndingPoint().getX() && e.getStartingPoint().getY() == e.getEndingPoint().getY()) {
                finishedEdges.remove(e);
                i--;
            }
        }
    }

    public List<Edge> getFinishedEdges() {
        return finishedEdges;
    }

    public void addNewKeyPoint(Point point) {
        keyPoints.add(point);
    }

    public void addNewKeyPoint(ArrayList<StatObject> keyObjects) {
        for (StatObject object : keyObjects) {
            keyPoints.add(new Point(object.getX(), object.getY()));
        }
    }

    public void setFinishedEdges(ArrayList<Edge> finishedEdges) {
        this.finishedEdges = finishedEdges;
    }

    public void setKeyPoints(ArrayList<Point> keyPoints) {
        this.keyPoints = keyPoints;
    }

}
