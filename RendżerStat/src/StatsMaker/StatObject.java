package StatsMaker;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class StatObject {
    private boolean isKeyObject;
    private boolean isContour;
    private boolean isDefinition;
    private String objectName;
    private ArrayList<Pair<String, Object>> properties;
    private double x;
    private double y;

    public StatObject(boolean isKeyObject, boolean isContour, boolean isDefinition, String objectName) {
        this.isKeyObject = isKeyObject;
        this.objectName = objectName;
        this.isContour = isContour;
        this.isDefinition = isDefinition;
        properties = new ArrayList<>();
    }

    public void addProperty(String label, Object value) {
        properties.add(new Pair<>(label, value));
    }

    public List<Pair<String, Object>> getProperties() {
        return properties;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public String getObjectName() {
        return objectName;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public boolean isKeyObject() {
        return isKeyObject;
    }

    public boolean isContour() {
        return isContour;
    }

    public boolean isNotDefinition() {
        return !isDefinition;
    }

    public StatObject copy() {
        boolean isKeyObject = this.isKeyObject;
        boolean isContour = this.isContour;
        boolean isDefinition = this.isDefinition;
        String name = this.objectName;
        StatObject object = new StatObject(isKeyObject, isContour, isDefinition, name);
        for (Pair<String, Object> pair : this.properties) {
            object.addProperty(pair.getKey(), pair.getValue());
        }
        return object;
    }

    @Override
    public int hashCode() {
        int hash = 71 * Double.valueOf(x).hashCode() + 91 * Double.valueOf(y).hashCode();
        if (isKeyObject) {
            return 37 + hash;
        } else if (isDefinition) {
            return 47 + hash;
        } else if (isContour) {
            return 57 + hash;
        } else {
            return 67 + hash;
        }
    }
}