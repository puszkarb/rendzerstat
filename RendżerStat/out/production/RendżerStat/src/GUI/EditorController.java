package GUI;

import StatsMaker.StatObject;
import Voronoi.Point;
import com.jfoenix.controls.*;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.io.File;
import java.util.ArrayList;

public class EditorController extends Controller {

    private ArrayList<StatObject> temporaryObjects = new ArrayList<>();
    private ArrayList<StatObject> temporaryTrashBin = new ArrayList<>();
    private Image background;

    @FXML
    protected JFXToggleButton editorToMainButton;
    @FXML
    protected JFXTextField objectNameTextField;
    @FXML
    protected JFXTextArea labelsTextArea;
    @FXML
    protected JFXCheckBox isKeyObjectCheckBox;
    @FXML
    protected ChoiceBox<String> objectChoiceBox;
    @FXML
    protected TableView<Pair> parametersTableView;
    @FXML
    protected TableColumn<Object, Object> paramTableColumn;
    @FXML
    protected TableColumn<Object, Object> valueTableColumn;
    @FXML
    protected Canvas editionStageCanvas;
    @FXML
    protected JFXTextField paramTextField;
    @FXML
    protected JFXTextField valueTextField;
    @FXML
    protected JFXButton insertButton;
    @FXML
    protected JFXButton changeRecordButton;
    @FXML
    protected JFXToggleButton deleteObjectToggleButton;
    @FXML
    protected ImageView imagePreview;
    @FXML
    protected Canvas editionStageCanvasBackground;
    @FXML
    protected JFXButton eraseContourPointButton;

    public void switchToMainStage() {
        this.application.mainStageController.setMainToggleButtonActive();
        deleteObjectToggleButton.setSelected(false);
        editionStageCanvas.setOnMouseClicked(doNothing());
        restoreTrash();

        if (application.dataReader.getKeyObjects().size() > 0) {
            refreshCanvas(application.mainStageController.mainStageCanvas, true);
            if (application.statsMaker.getFinishedEdges().size() > 0) {
                application.mainStageController.areaMaker.createNewAreas();
            }
        }
        application.mainStageController.getStatisticsOverAll();
        application.mainStageController.prepareCheckBoxes(application.dataReader.getObjectsDefinitions(), -1);
        application.mainStageController.show();
    }

    public void setEditorToggleButtonActive(boolean toggleButtonActive) {
        this.editorToMainButton.setSelected(toggleButtonActive);
    }

    public void addDefinition() {
        ArrayList<Pair<String, Object>> labels = new ArrayList<>();
        String name = objectNameTextField.getText().toUpperCase();
        String[] labelsTextSplit = labelsTextArea.getText().split(" ");

        if (name.equals("RóżowyRendżer".toUpperCase())) {
            System.out.println("easter:");
            easterEgg();
        }

        for (String word : labelsTextSplit) {
            System.out.println("---" + word);
        }
        if (labelsTextSplit.length % 2 == 0) {
            for (int i = 1; i < labelsTextSplit.length; i += 2) {
                if (labelsTextSplit[i].toLowerCase().equals("int") ||
                        labelsTextSplit[i].toLowerCase().equals("double") ||
                        labelsTextSplit[i].toLowerCase().equals("string")) {
                    labels.add(new Pair<>(labelsTextSplit[i - 1], labelsTextSplit[i]));
                } else {
                    Alert error = new Alert(Alert.AlertType.ERROR);
                    error.setTitle("Błąd");
                    error.setHeaderText("Podano niepoprawny typ danych: " + labelsTextSplit[i] + ".\nAkceptowane typy danych to String, Int oraz Double.");
                    error.showAndWait();
                    return;
                }
            }
        } else {
            Alert error = new Alert(Alert.AlertType.ERROR);
            error.setTitle("Błąd");
            error.setHeaderText("Podano niepoprawną definicję parametrów obiektu.\n Przykład poprawnej definicji: <NAZWA_PARAMETRY> <String/int/double>\nPodano: " + labelsTextArea.getText());
            error.showAndWait();
            return;
        }

        editor.addDefinition(false, isKeyObjectCheckBox.isSelected(), name, labels);
        refreshObjectChoiceBox();
        objectNameTextField.clear();
        labelsTextArea.clear();
        isKeyObjectCheckBox.setSelected(false);
    }

    private void easterEgg() {
        MediaPlayer mediaPlayer;
        Media mp3MusicFile = new Media(getClass().getResource("easteregg.mp3").toExternalForm());
        mediaPlayer = new MediaPlayer(mp3MusicFile);
        mediaPlayer.setVolume(0.9);
        mediaPlayer.play();
    }

    public void refreshObjectChoiceBox() {
        ArrayList<String> names = new ArrayList<>();

        for (StatObject object : editor.getDefinitions()) {
            names.add(object.getObjectName());
        }
        objectChoiceBox.setItems(FXCollections.observableArrayList(names));
        if (names.size() > 0) {
            objectChoiceBox.setValue(names.get(0));

        }
        objectChoiceBox.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            if (!(oldValue.intValue() < 0) && !(newValue.intValue() < 0)) {
                parametersTableView.getItems().clear();
                paramTextField.clear();
                valueTextField.clear();
                insertButton.setButtonType(JFXButton.ButtonType.RAISED);
                paramTableColumn.setCellValueFactory(new PropertyValueFactory<>("key"));
                valueTableColumn.setCellValueFactory(new PropertyValueFactory<>("value"));
                for (Pair<String, Object> pair : editor.getDefinitions().get(newValue.intValue()).getProperties()) {
                    if (!pair.getKey().toLowerCase().equals("x") && !pair.getKey().toLowerCase().equals("y")) {
                        parametersTableView.getItems().add(pair);
                    }
                }
            }
        });

        parametersTableView.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue != null && newValue != null) {
                try {
                    paramTextField.setText(paramTableColumn.getCellObservableValue(newValue.intValue()).getValue().toString());
                    paramTextField.setEditable(false);
                    valueTextField.setText(valueTableColumn.getCellObservableValue(newValue.intValue()).getValue().toString());
                } catch (Exception ignored) {
                }
            }
        });
    }

    public void changeStat() {
        int indexOfDefinition = objectChoiceBox.getSelectionModel().getSelectedIndex();
        int indexInColumn = parametersTableView.getSelectionModel().selectedIndexProperty().intValue();

        if (indexInColumn < 0 || indexOfDefinition < 0) {
            application.notificationStageController.notificationWindow("Nie wybrano parametru do zmiany.");
        } else {
            StatObject definition = editor.getDefinitions().get(indexOfDefinition);

            String paramName = paramTableColumn.getCellObservableValue(indexInColumn).getValue().toString();
            System.out.println(paramName);
            int indexOfParamInParamsListOfDefinition;
            for (indexOfParamInParamsListOfDefinition = 0; indexOfParamInParamsListOfDefinition < definition.getProperties().size(); indexOfParamInParamsListOfDefinition++) {
                if (definition.getProperties().get(indexOfParamInParamsListOfDefinition).getKey().toLowerCase().equals(paramName.toLowerCase())) {
                    break;
                }
                System.out.println(definition.getProperties().get(indexOfParamInParamsListOfDefinition).getValue().toString().toLowerCase());
            }

            String type = definition.getProperties().get(indexOfParamInParamsListOfDefinition).getValue().toString();

            String label = editor.getDefinitions().get(indexOfDefinition).getProperties().get(indexOfParamInParamsListOfDefinition).getKey();
            System.out.println(editor.getDefinitions().get(indexOfDefinition).getObjectName() + "     " + editor.getDefinitions().get(indexOfDefinition).getProperties().get(indexOfParamInParamsListOfDefinition).getKey() + " " + type);

            switch (type.toLowerCase()) {
                case "int":
                    try {
                        int newIntValue = Integer.valueOf(valueTextField.getText());
                        System.out.println(parametersTableView.getItems() == null);
                        parametersTableView.getItems().set(indexInColumn, new Pair<>(label, newIntValue));
                        System.out.println(newIntValue);
                    } catch (Exception e) {
                        showError("Integer", valueTextField.getText(), 1);
                    }

                    break;

                case "double":
                    try {
                        double newDoubleValue = Double.valueOf(valueTextField.getText());
                        System.out.println(parametersTableView.getItems() == null);
                        parametersTableView.getItems().set(indexInColumn, new Pair<>(label, newDoubleValue));
                        System.out.println(newDoubleValue);
                    } catch (Exception e) {
                        showError("Double", valueTextField.getText(), 1);
                    }
                    break;

                case "string":
                    try {
                        String newStringValue = valueTextField.getText();
                        System.out.println(parametersTableView.getItems() == null);
                        parametersTableView.getItems().set(indexInColumn, new Pair<>(label, newStringValue));
                        System.out.println(newStringValue);
                    } catch (Exception e) {
                        showError("String", valueTextField.getText(), 1);
                    }
                    break;

                default:
                    break;
            }
            insertButton.setButtonType(JFXButton.ButtonType.RAISED);
            editionStageCanvas.setOnMouseClicked(doNothing());
        }
    }

    public void createNewObject() {
        if (insertButton.getButtonType() == JFXButton.ButtonType.RAISED) {
            insertButton.setButtonType(JFXButton.ButtonType.FLAT);
            String definition_name = objectChoiceBox.getValue();
            int i;
            for (i = 0; i < editor.getDefinitions().size(); i++) {
                if (editor.getDefinitions().get(i).getObjectName().equals(definition_name)) {
                    break;
                }
            }
            int errorFlag = 0;

            StatObject definition = editor.getDefinitions().get(i);
            StatObject newObject = new StatObject(definition.isKeyObject(), false, false, definition_name);

            for (int j = 0; j < definition.getProperties().size(); j++) {
                if (errorFlag != 0) {
                    break;
                }
                if (!definition.getProperties().get(j).getKey().toLowerCase().equals("x") &&
                        !definition.getProperties().get(j).getKey().toLowerCase().equals("y")) {

                    int id;
                    for (id = 0; id < parametersTableView.getItems().size(); id++) {
                        newObject.addProperty(definition.getProperties().get(j).getKey(), valueTableColumn.getCellObservableValue(id).getValue());
                        System.out.println(definition.getProperties().get(j).getKey() + " " + valueTableColumn.getCellObservableValue(id).getValue());

                        switch (definition.getProperties().get(j).getValue().toString().toLowerCase()) {
                            case "int":
                                if (newObject.getProperties().get(id).getValue().getClass() != Integer.class) {
                                    application.notificationStageController.notificationWindow("Wstawiona wartość " + valueTableColumn.getCellObservableValue(id).getValue().toString() + " nie jest typu Integer");

                                    errorFlag++;
                                }
                                break;

                            case "double":
                                if (newObject.getProperties().get(id).getValue().getClass() != Double.class) {
                                    System.out.println(newObject.getProperties().get(newObject.getProperties().size() - 1).getValue().getClass());
                                    application.notificationStageController.notificationWindow("wstawiona wartość " + valueTableColumn.getCellObservableValue(id).getValue().toString() + " nie jest typu Double");
                                    errorFlag++;
                                }
                                break;

                            case "string":
                                if (newObject.getProperties().get(id).getValue().getClass() != String.class) {
                                    application.notificationStageController.notificationWindow("wstawiona wartość " + valueTableColumn.getCellObservableValue(id).getValue().toString() + " nie jest typu String");

                                    errorFlag++;
                                }
                                break;

                            default:
                                application.notificationStageController.notificationWindow("Nieznany typ danych");
                                errorFlag++;

                        }
                    }
                }
            }

            GraphicsContext gc = editionStageCanvas.getGraphicsContext2D();
            editionStageCanvas.setOnMouseClicked(event -> {
                StatObject anotherNewObject = newObject.copy();

                anotherNewObject.setX(event.getX());
                anotherNewObject.setY(event.getY());
                editor.addObject(anotherNewObject);
                if (anotherNewObject.isKeyObject()) {
                    gc.setFill(Color.RED);
                    temporaryObjects.add(anotherNewObject);
                } else {
                    gc.setFill(Color.GREEN);
                    //temporaryObjects.add(anotherNewObject);
                }
                gc.fillOval(event.getX(), event.getY(), 5, 5);
            });
        } else {
            insertButton.setButtonType(JFXButton.ButtonType.RAISED);
            editionStageCanvas.setOnMouseClicked(doNothing());
        }
    }

    public void createContourPoint() {
        GraphicsContext gc = editionStageCanvas.getGraphicsContext2D();
        editionStageCanvas.setOnMouseClicked(event -> {
            StatObject newContour = new StatObject(false, true, false, "contourpoint" +
                    application.dataReader.getContours().size());
            newContour.setX(event.getX());
            newContour.setY(event.getY());

            temporaryObjects.add(newContour);
            gc.setFill(Color.BLUEVIOLET);
            gc.fillOval(event.getX(), event.getY(), 5, 5);
        });
    }

    public void saveEdition() {
        for (StatObject o : temporaryObjects) {
            if (o.isKeyObject()) {
                if (!application.dataReader.addKeyObject(o)) {
                    Alert errorAlert = new Alert(Alert.AlertType.WARNING);
                    errorAlert.setHeaderText("Punkt kluczowy o wybranych współrzędnych już istnieje!");
                    errorAlert.setContentText("Nie można dodać duplikatu.");
                    errorAlert.showAndWait();
                    continue;
                }
                application.statsMaker.addNewKeyPoint(new Point(o.getX(), o.getY()));
            } else if (o.isContour()) {
                application.dataReader.addContour(o);
            } else {
                application.dataReader.addObject(o);
            }
        }
        application.dataReader.refreshKeyObjectList();
        application.dataReader.checkContours();
        editionStageCanvas.setOnMouseClicked(doNothing());
        if (background != null) {
            fitImageOn(application.mainStageController.mainStageCanvasBackground, background);
        }
        if (checkIfOutContour(application.dataReader.getObjects())) {
            restoreTrash();
            return;
        }
        temporaryObjects = new ArrayList<>();
        temporaryTrashBin = new ArrayList<>();
        refreshCanvas(editionStageCanvas, false);
    }

    public void eraseObject() {
        if (deleteObjectToggleButton.isSelected()) {
            editionStageCanvas.setOnMouseClicked(event -> {
                double min_distance = Double.MAX_VALUE;
                double distance;
                StatObject chosenObject = null;
                if (!editor.getObjects().isEmpty()) {
                    for (StatObject object : editor.getObjects()) {
                        distance = Math.sqrt(Math.pow((object.getX() - event.getX()), 2) + Math.pow((object.getY() - event.getY()), 2));
                        if (distance < min_distance) {
                            min_distance = distance;
                            chosenObject = object;
                            System.out.println("Wybrano: " + chosenObject.getObjectName() + " X:" + chosenObject.getX() + " Y:" + chosenObject.getY());
                        }
                    }
                }

                if (chosenObject != null) {

                    if (chosenObject.isKeyObject()) {
                        System.out.println("Key Object --- >" + application.dataReader.getKeyObjects().contains(chosenObject));
                        if (application.dataReader.getKeyObjectsMap().size() == 1) {
                            Alert error = new Alert(Alert.AlertType.ERROR);
                            error.setTitle("Nieprawidłowe działanie");
                            error.setHeaderText("Nie można usunąć ostaniego punktu kluczowego!");
                            error.showAndWait();
                            return;
                        }
                        application.dataReader.getKeyObjectsMap().remove(chosenObject.hashCode());
                        application.dataReader.getKeyObjects().remove(chosenObject);

                    } else if (!chosenObject.isContour()) {
                        application.dataReader.getObjects().remove(chosenObject);
                    }
                    editor.getObjects().remove(chosenObject);
                    temporaryTrashBin.add(chosenObject);
                }

                refreshCanvas(editionStageCanvas, false);
            });
        } else {
            editionStageCanvas.setOnMouseClicked(doNothing());
        }
    }

    private void restoreTrash() {
        if (!temporaryTrashBin.isEmpty()) {
            for (StatObject object : temporaryTrashBin) {
                editor.addObject(object);
                if (object.isKeyObject()) {
                    application.dataReader.addKeyObject(object);
                } else if (object.isContour()) {
                    application.dataReader.getContours().add(object);
                } else {
                    application.dataReader.getObjects().add(object);
                }
            }
        }
        temporaryTrashBin = new ArrayList<>();
    }

    private void showError(String expected, String given, int type) {
        Alert error = new Alert(Alert.AlertType.ERROR);
        switch (type) {
            case 1:
                error.setHeaderText("Podane dane w polu edycji wartości są nieprawidłowe!");
                error.setContentText("Podano: " + given + ".\nOczekiwany typ: " + expected + ".");
                error.showAndWait();
                break;
        }
    }

    public void setGraphic() {
        if (background == null) {
            showImageError();
            return;
        }
        fitImageOn(editionStageCanvasBackground, background);
    }

    private void showImageError() {
        Alert imageAlert = new Alert(Alert.AlertType.ERROR);
        imageAlert.setHeaderText("Podano nieprawidłowy plik graficzny!");
        imageAlert.setTitle("Błąd pliku tła");
        imageAlert.setContentText("Upewnij się, że program może otworzyć plik.\nWymagany jest format graficzny.");
        imageAlert.showAndWait();
    }

    public void openFileChooserForGraphics() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Otwórz Plik Graficzny");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(".png", "*.png"));
        File backgroundImageFile = fileChooser.showOpenDialog(new Stage());
        if (backgroundImageFile != null) {
            background = new Image(backgroundImageFile.toURI().toString());
            imagePreview.setImage(background);
        }
    }

    private void fitImageOn(Canvas canvas, Image image) {
        if (image.isError()) {
            showImageError();
            return;
        }

        double imageAspectRatio = image.getWidth() / image.getHeight();
        double canvasAspectRatio = canvas.getWidth() / canvas.getHeight();
        double renderableHeight, renderableWidth, startX, startY;

        if (imageAspectRatio > canvasAspectRatio) {
            renderableWidth = canvas.getWidth();
            renderableHeight = image.getHeight() * (renderableWidth / image.getWidth());
            startX = 0;
            startY = (canvas.getHeight() - renderableHeight) / 2;
        } else {
            renderableHeight = canvas.getHeight();
            renderableWidth = canvas.getWidth();
            startX = 0;
            startY = 0;
        }
        canvas.getGraphicsContext2D().drawImage(image, startX, startY, renderableWidth, renderableHeight);
    }

    public void eraseContourPoint() {
        editionStageCanvas.setOnMouseClicked(event -> {
            ArrayList<StatObject> contours = application.dataReader.getContours();
            if (contours.size() > 0) {
                double min_distance = Double.MAX_VALUE;
                int index = 0;
                for (int i = 0; i < contours.size(); i++) {
                    double distance = Math.sqrt(Math.pow((event.getX() - contours.get(i).getX()), 2) + Math.pow((event.getY() - contours.get(i).getY()), 2));
                    if (distance < min_distance) {
                        min_distance = distance;
                        index = i;
                    }
                }
                contours.remove(index);
                refreshCanvas(editionStageCanvas, false);
            }
        });
    }

}
