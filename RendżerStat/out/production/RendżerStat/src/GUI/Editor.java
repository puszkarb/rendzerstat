package GUI;

import StatsMaker.StatObject;
import javafx.util.Pair;

import java.util.ArrayList;

public class Editor {

    private ArrayList<StatObject> objects = new ArrayList<>();
    private ArrayList<StatObject> definitions = new ArrayList<>();
    private static final Editor instance = new Editor();

    private Editor() {
    }

    public static Editor getInstance() {
        return instance;
    }

    public void setObjects(ArrayList<StatObject> objects) {
        instance.objects = objects;
    }

    public ArrayList<StatObject> getObjects() {
        return instance.objects;
    }

    public void setDefinitions(ArrayList<StatObject> definitions) {
        instance.definitions = definitions;
    }

    public ArrayList<StatObject> getDefinitions() {
        return definitions;
    }

    public void addDefinition(boolean isContour, boolean isKeyObject, String name, ArrayList<Pair<String, Object>> labels) {
        if (labels.size() > 0) {
            StatObject object = new StatObject(isKeyObject, isContour, true, name);
            for (Pair<String, Object> pair : labels) {
                object.addProperty(pair.getKey(), pair.getValue());
            }
            System.out.println("Dodaje do definicji");
            definitions.add(object);

        }
    }

    public void addObject(StatObject object) {
        objects.add(object);
    }
}
