package DataReader;

import ConvexHull.ConvexHull;
import GUI.Editor;
import StatsMaker.StatObject;
import Voronoi.ContourLine;
import Voronoi.Point;
import javafx.util.Pair;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Data {
    private ArrayList<StatObject> objects;
    private ArrayList<StatObject> contours;
    private ArrayList<StatObject> keyObjects;
    private HashMap<Integer, StatObject> keyObjectsMap;
    private ArrayList<StatObject> objectsDefinitions;
    private Editor editor;
    private int errorLine;

    public Data() {
        objects = new ArrayList<>();
        contours = new ArrayList<>();
        keyObjects = new ArrayList<>();
        keyObjectsMap = new HashMap<>();
        objectsDefinitions = new ArrayList<>();
        editor = Editor.getInstance();
    }

    public int readFile(File file, boolean returnFromNotification) {
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            int lineNumber = 0;
            int lastInterpretedComment = -1;
            String[] separated;

            if (returnFromNotification) {
                for (int i = 0; i < errorLine; i++) {
                    bufferedReader.readLine();
                    lineNumber++;
                }
                lastInterpretedComment = 1; //działa tylko dla keyPoint!
            }

            while ((line = bufferedReader.readLine()) != null) {

                lineNumber++;
                if (line.matches("#.*?")) {
                    System.out.println("Koment");

                    // robimy tutaj rozpoznawanie linii

                    separated = line.split(" ");
                    lastInterpretedComment = lineInterpreter(separated);
                } else {
                    separated = line.split(" ");
                    if (separated.length != 0) {
                        switch (lastInterpretedComment) {
                            case 0:
                                if (!createNewContourPoint(separated)) {
                                    errorLine = lineNumber;
                                    return 2;
                                }
                                break;
                            case 1:
                                int result = createNewKeyPoint(separated);
                                if (result != 0 || createNewObjectDefinitionError(separated, true)) {
                                    errorLine = lineNumber;
                                    if (result == 420) {
                                        return 420;
                                    } else if (result == 888) {
                                        return 888;
                                    } else if (result == 966) {
                                        return 966;
                                    }
                                    return 1;
                                }
                                break;
                            case 2:
                                if (createNewObjectDefinitionError(separated, false)) {
                                    errorLine = lineNumber;
                                    return 3;
                                }
                                break;
                            case 3:
                                int resultAns = createNewObject(separated);
                                if (resultAns != 0) {
                                    errorLine = lineNumber;
                                    if (resultAns == 1998) {
                                        return 345;
                                    } else if (resultAns == 1999) {
                                        return 356;
                                    }
                                    return 7;
                                }
                                break;
                            default:
                                errorLine = lineNumber;
                                System.err.println("Niepoprawne dane");
                                return 997;
                        }
                    }
                }
            }
            keyObjects = new ArrayList<>(keyObjectsMap.values());
            editor.setDefinitions(objectsDefinitions);
            editor.setObjects(objects);
            bufferedReader.close();
        } catch (IOException e) {
            return 666;
        }
        return 0;
    }

    private int createNewObject(String[] separated) {
        int typeExist = 0;
        if (separated.length > 2) {
            StatObject object = new StatObject(false, false, false, separated[1]);

            for (StatObject statObject : objectsDefinitions) {
                if (statObject.getObjectName().equals(separated[1])) {
                    typeExist++;

                    for (int j = 0; j < statObject.getProperties().size(); j++) {
                        Pair<String, Object> property = statObject.getProperties().get(j);

                        switch (property.getValue().toString().toLowerCase()) {
                            case "string":
                                String joined = String.join(" ", separated);
                                System.out.println(joined);
                                String[] quoted = joined.split("\"");
                                if (quoted.length == 1) {
                                    return 1998;
                                }
                                object.addProperty(property.getKey(), quoted[1]);   //Dla potomnych: biorę quoted[1] bo jest to pierwszy napis zawarty między "..."
                                quoted[1] = ".";
                                separated = String.join("", quoted).split(" ");
                                // narazie tylko dla jedengo string
                                break;
                            case "double":
                                object.addProperty(property.getKey(), Double.valueOf(separated[2 + j]));
                                break;
                            case "int":
                                object.addProperty(property.getKey(), Integer.valueOf(separated[2 + j]));
                                break;
                        }

                        if (property.getKey().toLowerCase().equals("x")) {
                            object.setX((Double) object.getProperties().get(j).getValue());
                        }
                        if (property.getKey().toLowerCase().equals("y")) {
                            object.setY((Double) object.getProperties().get(j).getValue());
                        }
                    }
                }
            }
            if (typeExist > 0) {
                objects.add(object);
                editor.addObject(object);
            } else {
                System.err.println("Nie istniejący typ obietu");
                return 1999;
            }
            return 0;
        }
        return 2000;
    }

    private boolean createNewObjectDefinitionError(String[] separated, boolean isKeyObject) {
        if (!isKeyObject) {
            if (separated.length > 3 && separated.length % 2 == 0) {  //Warunek parzystości konieczny, ponieważ mówimy o parach label - value
                StatObject objectDefinition = new StatObject(false, false, true, separated[1]);

                for (int i = 2; i < separated.length; i += 2) {
                    if (separated[i + 1].equals("String") || separated[i + 1].equals("int") || separated[i + 1].equals("double")) {
                        objectDefinition.addProperty(separated[i], separated[i + 1]);
                    } else {
                        return true;
                    }
                }

                objectsDefinitions.add(objectDefinition);
                return false;
            } else if (separated.length == 1) {
                return false; //????
            } else {
                System.err.println("Napotkano niepoprawne dane podczas interpretowania definicji obiektów");
                return true;
            }
        } else {
            String name = buildName(separated);
            if (!(name.length() < 2)) {
                StatObject keyObjectDefinition = new StatObject(true, false, true, name);
                keyObjectDefinition.addProperty("X", "double");
                keyObjectDefinition.addProperty("Y", "double");
                objectsDefinitions.add(keyObjectDefinition);
                return false;
            }
            return false;
        }
    }

    private int createNewKeyPoint(String[] separated) {
        if (separated.length > 3) {
            String name = buildName(separated);
            StatObject keyPoint = new StatObject(true, false, false, name);
            try {
                return addPointToMap(separated, keyPoint);
            } catch (NumberFormatException e) {
                return 888;
            }
        } else if (separated.length == 1) {
            return 0; //???
        } else {
            System.err.println("Napotkano niepoprawne dane podczas interpretowania punktów kluczowych");
            return 997;
        }
    }

    private String buildName(String[] separatedLine) {
        StringBuilder nameBuilder = new StringBuilder();
        for (int i = 3; i < separatedLine.length; i++) {
            nameBuilder.append(separatedLine[i]).append(" ");
        }
        return nameBuilder.toString();
    }

    private boolean createNewContourPoint(String[] separated) {
        if (separated.length == 3) {
            StatObject contourPoint = new StatObject(false, true, false, "ContourPoint" + separated[0]);
            try {
                return addPointToList(contours, separated, contourPoint);
            } catch (NumberFormatException e) {
                return false;
            }
        } else if (separated.length == 1) {
            return true;
        } else {
            System.err.println("Napotkano niepoprawne dane podczas interpretowania konturów terenu");
            return false;
        }
    }

    private boolean setStatObjectXYError(String[] separatedLine, StatObject object) {
        double x = Double.valueOf(separatedLine[1]);
        double y = Double.valueOf(separatedLine[2]);
        if (x < 0 || y < 0) {
            return true;
        }
        object.setX(x);
        object.setY(y);
        return false;
    }

    private int addPointToMap(String[] separatedLine, StatObject keyPoint) {
        if (setStatObjectXYError(separatedLine, keyPoint)) {
            return 420;
        }
        if (keyObjectsMap.containsKey(keyPoint.hashCode())) {
            return 966;
        }
        objects.add(keyPoint);
        keyObjects.add(keyPoint);
        keyObjectsMap.put(keyPoint.hashCode(), keyPoint);
        return 0;
    }

    private boolean addPointToList(List<StatObject> list, String[] separatedLine, StatObject object) {
        if (setStatObjectXYError(separatedLine, object)) {
            return false;
        }
        list.add(object);
        editor.addObject(object);
        return true;
    }

    private int lineInterpreter(String[] separated) {
        switch (separated[1] + " " + separated[2]) {
            case "Kontury terenu":
                System.out.println("Następne linie to kontury\n");
                return 0;
            case "Punkty kluczowe:":
                System.out.println("Następne linie to punkty kluczowe\n");
                return 1;
            case "Definicje obiektów:":
                System.out.println("Następne linie to definicje obiektów\n");
                return 2;
            case "Obiekty: Typ_obiektu":
                System.out.println("Następne linie to obiekty\n");
                return 3;
            default:
                System.out.println("Nie wiem ki chuj wczytałem -> pewnie błąd");
                return -1;
        }
    }

    public HashMap<Integer, StatObject> getKeyObjectsMap() {
        return keyObjectsMap;
    }

    public ArrayList<StatObject> getKeyObjects() {
        return keyObjects;
    }

    public ArrayList<StatObject> getObjectsDefinitions() {
        return objectsDefinitions;
    }

    public ArrayList<StatObject> getObjects() {
        return objects;
    }

    public void addContour(StatObject statObject) {
        contours.add(statObject);
    }

    public ArrayList<StatObject> getContours() {
        return contours;
    }

    public int getErrorLine() {
        return errorLine;
    }

    public boolean addKeyObject(StatObject o) {
        if (keyObjectsMap.containsKey(o.hashCode())) {
            return false;
        }
        keyObjectsMap.put(o.hashCode(), o);

        return true;
    }

    public void refreshKeyObjectList() {
        keyObjects = new ArrayList<>(keyObjectsMap.values());
    }

    public void addObject(StatObject o) {
        objects.add(o);
    }

    public void save(File file) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        List<StatObject> keyObjects = new ArrayList<>(keyObjectsMap.values());

        String commentary = "# Kontury terenu (wymienione w kolejności łączenia): Lp. x y\n";
        writer.write(commentary);
        for (int i = 0; i < contours.size(); i++) {
            writer.write(i + ". " + contours.get(i).getX() + " " + contours.get(i).getY() + "\n");
        }
        writer.write("\n");
        commentary = "# Punkty kluczowe: Lp. x y Nazwa\n";
        writer.write(commentary);
        for (int i = 0; i < keyObjects.size(); i++) {
            writer.write(i + ". " + keyObjects.get(i).getX() + " " + keyObjects.get(i).getY() + " " + keyObjects.get(i).getObjectName() + "\n");
        }
        writer.write("\n");
        commentary = "# Definicje obiektów: Lp. Typ_obiektu (Nazwa_zmiennej Typ_zmiennej)\n";
        writer.write(commentary);
        writeProperties(writer, objectsDefinitions);
        writer.write("\n");
        commentary = "# Obiekty: Typ_obiektu (zgodnie z definicją)\n";
        writer.write(commentary);
        writeProperties(writer, objects);
        writer.close();
    }

    private void writeProperties(BufferedWriter writer, ArrayList<StatObject> objectList) throws IOException {
        String properties;
        for (int i = 0; i < objectList.size(); i++) {
            properties = preparePropertiesDescription(i, objectList);
            writer.write(i + ". " + objectList.get(i).getObjectName() + " " + properties + "\n");
        }
    }

    private String preparePropertiesDescription(int i, ArrayList<StatObject> list) {
        String result;
        StringBuilder builder = new StringBuilder();
        if (list == objectsDefinitions) {
            for (Pair<String, Object> pair : list.get(i).getProperties()) {
                builder.append(pair.getKey()).append(" ").append(pair.getValue().toString()).append(" ");
            }
        } else {
            for (Pair<String, Object> pair : list.get(i).getProperties()) {
                if (pair.getValue().getClass() == String.class) {
                    builder.append("\"");
                }
                builder.append(pair.getValue().toString()).append(" ");
                if (pair.getValue().getClass() == String.class) {
                    builder.append("\" ");
                }
            }
        }
        result = builder.toString();
        return result;
    }

    public void checkContours() {
        ArrayList<StatObject> onlyConvexHull = new ArrayList<>();
        ArrayList<Point> pointsFromContours = new ArrayList<>();
        ArrayList<Point> selectedPoints;

        for (StatObject contour : contours) {
            pointsFromContours.add(new Point(contour.getX(), contour.getY()));
        }
        ConvexHull convexHull = new ConvexHull();
        selectedPoints = convexHull.FindConvexHull(pointsFromContours);

        for (Point p : selectedPoints) {
            for (StatObject contour : contours) {
                if (p.getX() == contour.getX() && p.getY() == contour.getY()) {
                    onlyConvexHull.add(contour);
                }
            }
        }
        contours = onlyConvexHull;
    }

    public List<ContourLine> getContourLines() {
        List<ContourLine> contourLines = new ArrayList<>();
        if (contours.size() == 0) {
            return null;
        }
        for (int i = 1; i < contours.size(); i++) {
            Point contourLineStart = new Point(contours.get(i - 1).getX(), contours.get(i - 1).getY());
            Point contourLineEnd = new Point(contours.get(i).getX(), contours.get(i).getY());
            ContourLine contourLine = new ContourLine(contourLineStart, contourLineEnd);
            contourLines.add(contourLine);
        }
        Point lastContourStart = new Point(contours.get(contours.size() - 1).getX(), contours.get(contours.size() - 1).getY());
        Point lastContourEnd = new Point(contours.get(0).getX(), contours.get(0).getY());
        ContourLine lastContour = new ContourLine(lastContourStart, lastContourEnd);
        contourLines.add(lastContour);
        return contourLines;
    }

    public void removeKeyObjectFromKeyObjectList(StatObject keyPoint) {
        keyObjects.remove(keyPoint);
        objects.remove(keyPoint);
        keyObjectsMap.remove(keyPoint.hashCode());
    }
}
