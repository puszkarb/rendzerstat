package StatsMaker;

import Voronoi.Point;
import javafx.scene.paint.Color;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class Area {

    private StatObject keyPoint;
    private ArrayList<StatObject> objects;
    private List<Pair<String, Double>> quantityOfProperties;
    private ArrayList<Point> contours;
    private Color color;
    private int index;

    public Area() {
        objects = new ArrayList<>();
        contours = new ArrayList<>();
        quantityOfProperties = new ArrayList<>();
    }

    public StatObject getKeyPoint() {
        return keyPoint;
    }

    public void setKeyPoint(StatObject keyPoint) {
        this.keyPoint = keyPoint;
    }

    public void setContours(ArrayList<Point> contours) {
        this.contours = contours;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public List<Pair<String, Double>> getQuantityOfProperties() {
        return quantityOfProperties;
    }

    public void printAreaDescription() {
        System.out.println("ID: " + index + "\tNazwa Obiektu Kluczowego: " + keyPoint.getObjectName());
        System.out.println("Ending Pointy należące do obszaru:");
        for (Point p : contours) {
            System.out.println("\tX: " + p.getX() + "  Y:" + p.getY());
        }
    }

    public void assignObject(StatObject object) {
        objects.add(object);
    }

    public ArrayList<StatObject> getObjects() {
        return objects;
    }
}
