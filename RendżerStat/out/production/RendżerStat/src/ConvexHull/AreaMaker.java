package ConvexHull;

import DataReader.Data;
import StatsMaker.Area;
import StatsMaker.StatObject;
import StatsMaker.StatsMaker;
import Voronoi.Edge;
import Voronoi.Point;
import javafx.scene.paint.Color;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class AreaMaker {
    private Data data;
    private StatsMaker statsMaker;
    private ArrayList<Area> areasList;
    private ArrayList<ArrayList<Point>> contoursForEachKeyObject;
    private HashMap<Integer, Point> points;
    public ArrayList<Point> pointsArrayList;

    public AreaMaker(Data data, StatsMaker statsMaker) {
        this.data = data;
        this.statsMaker = statsMaker;
        areasList = new ArrayList<>();
        contoursForEachKeyObject = new ArrayList<>();
        points = new HashMap<>();
        pointsArrayList = new ArrayList<>();
    }

    public void createNewAreas() {
        findContours();
        Random generator = new Random();
        System.out.println("createNewAreas - rozmiar contoursForEachKeyObject -- " + contoursForEachKeyObject.size());
        for (int i = 0; i < contoursForEachKeyObject.size() && i < data.getKeyObjects().size(); i++) {
            Area area = new Area();
            area.setKeyPoint(data.getKeyObjects().get(i));
            area.setContours(contoursForEachKeyObject.get(i));
            double r = generator.nextDouble();
            double g = generator.nextDouble();
            double b = generator.nextDouble();
            area.setColor(new Color(r, g, b, 0.25));
            area.setIndex(i);

            areasList.add(area);
        }

        assignObjectsToArea(data.getObjects());
    }

    private void findContours() {
        fillPoints();
        clear();

        ArrayList<ArrayList<Double>> distancesForKeyPoints = new ArrayList<>();
        for (int i = 0; i < data.getKeyObjects().size(); i++) {
            distancesForKeyPoints.add(i, new ArrayList<>());
            contoursForEachKeyObject.add(new ArrayList<>());
        }
        for (Point p : pointsArrayList) {

            for (int i = 0; i < data.getKeyObjects().size(); i++) {
                distancesForKeyPoints.get(i).add(calculateDistance(p, data.getKeyObjects().get(i)));
            }
        }
        for (int pointOfEdgesIntersection = 0; pointOfEdgesIntersection < pointsArrayList.size(); pointOfEdgesIntersection++) {
            for (int indexOfKeyObject = 0; indexOfKeyObject < distancesForKeyPoints.size() - 1; indexOfKeyObject++) {
                for (int anotherObject = indexOfKeyObject + 1; anotherObject < distancesForKeyPoints.size(); anotherObject++) {
                    if (distancesForKeyPoints.get(indexOfKeyObject).get(pointOfEdgesIntersection).doubleValue() == distancesForKeyPoints.get(anotherObject).get(pointOfEdgesIntersection).doubleValue()) {
                        if (!contoursForEachKeyObject.get(indexOfKeyObject).contains(pointsArrayList.get(pointOfEdgesIntersection))) {
                            contoursForEachKeyObject.get(indexOfKeyObject).add(pointsArrayList.get(pointOfEdgesIntersection));
                        }
                        if (!contoursForEachKeyObject.get(anotherObject).contains(pointsArrayList.get(pointOfEdgesIntersection))) {
                            contoursForEachKeyObject.get(anotherObject).add(pointsArrayList.get(pointOfEdgesIntersection));
                        }
                    }
                }
            }
        }
    }

    private void clear() {
        areasList = new ArrayList<>();
        contoursForEachKeyObject = new ArrayList<>();
        points = new HashMap<>();
        pointsArrayList = new ArrayList<>();
    }


    private void assignObjectsToArea(ArrayList<StatObject> objects) {
        for (StatObject object : objects) {
            double[] distances = new double[data.getKeyObjects().size()];
            double min = Double.MAX_VALUE;
            int min_index = 0;
            for (int i = 0; i < data.getKeyObjects().size(); i++) {
                distances[i] = calculateDistance(new Point(object.getX(), object.getY()), data.getKeyObjects().get(i));
                if (distances[i] < min) {
                    min_index = i;
                    min = distances[i];
                }
            }
            areasList.get(min_index).assignObject(object);
            if (!object.isKeyObject() && !object.isContour() && !object.isDefinition()) {
                Area currentArea = areasList.get(min_index);
                List<Pair<String, Double>> areaProperties = currentArea.getQuantityOfProperties();

                for (Pair<String, Object> property : object.getProperties()) {
                    if (property.getKey().equals("X") || property.getKey().equals("Y") || property.getKey().equals("Nazwa")) {
                        continue;
                    }
                    if (areaProperties.isEmpty()) {
                        try {
                            areaProperties.add(new Pair<>(property.getKey(), ((Double) property.getValue())));
                        } catch (ClassCastException e) {
                            try {
                                areaProperties.add(new Pair<>(property.getKey(), Double.valueOf((Integer) property.getValue())));
                            } catch (ClassCastException e1) {
                            }
                        }
                    } else {
                        String name = property.getKey();
                        boolean isElementToBeAdded = true;
                        try {
                            for (int i = 0; i < areaProperties.size(); i++) {
                                String currentPropertyName = areaProperties.get(i).getKey();
                                if (name.equals(currentPropertyName)) {
                                    double newQuantity = areaProperties.get(i).getValue() + ((Double) property.getValue());
                                    areaProperties.set(i, new Pair<>(name, newQuantity));
                                    isElementToBeAdded = false;
                                }
                            }
                            if (isElementToBeAdded) {
                                areaProperties.add(new Pair<>(property.getKey(), (Double) property.getValue()));
                            }
                        } catch (ClassCastException e) {
                            try {
                                areaProperties.add(new Pair<>(property.getKey(), Double.valueOf((Integer) property.getValue())));
                            } catch (ClassCastException e1) {
                            }
                        }
                    }
                }
            }
        }
    }

    private void fillPoints() {
        for (Edge e : statsMaker.getFinishedEdges()) {
            if (!points.containsKey(e.getStartingPoint().hashCode())) {
                points.put(e.getStartingPoint().hashCode(), e.getStartingPoint());
                pointsArrayList.add(e.getStartingPoint());
            }
            if (!points.containsKey(e.getEndingPoint().hashCode())) {
                points.put(e.getEndingPoint().hashCode(), e.getEndingPoint());
                pointsArrayList.add(e.getEndingPoint());
            }
        }
    }

    private double calculateDistance(Point point, StatObject keyPoint) {
        double distance = Math.sqrt(Math.pow((point.getX() - keyPoint.getX()), 2) + Math.pow(((point.getY() - keyPoint.getY())), 2));
        return (double) Math.round(distance * 100) / 100;
    }

    public ArrayList<Area> getAreasList() {
        return areasList;
    }
}
