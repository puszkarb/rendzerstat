package ConvexHull;

import Voronoi.Point;

import java.util.*;

public class ConvexHull {

    private enum Turn {CLOCKWISE, COUNTER_CLOCKWISE, LINEAR}

    public ArrayList<Point> findInnerPointsOfTheArea(ArrayList<Point> points) {

        ArrayList<Point> result = new ArrayList<>(points);
        result = FindConvexHull(result);
        ArrayList<Point> final_result = new ArrayList<>();

        for (Point p : points) {
            if (!result.contains(p)) {
                final_result.add(p);
            }
        }
        return final_result;
    }

    public ArrayList<Point> FindConvexHull(ArrayList<Point> points) {
        List<Point> sortedPoints = new ArrayList<>(sortPoints(points));

        if (sortedPoints.size() < 3) {
            return new ArrayList<>();
        }

        Stack<Point> stack = new Stack<>();
        stack.push(sortedPoints.get(0));
        stack.push(sortedPoints.get(1));
        stack.push(sortedPoints.get(2));

        for (int i = 3; i < sortedPoints.size(); i++) {

            while (getTurn(getNextToTop(stack), stack.peek(), sortedPoints.get(i)) != Turn.COUNTER_CLOCKWISE) {
                stack.pop();
            }
            stack.push(sortedPoints.get(i));
        }

        return new ArrayList<>(stack);
    }

    private Point findTheLowestPointInArea(List<Point> points) {
        Point theLowestPoint = points.get(0);
        for (int i = 1; i < points.size(); i++) {
            Point temporary = points.get(i);
            if (temporary.getY() < theLowestPoint.getY() || (temporary.getY() == theLowestPoint.getY() && temporary.getX() < theLowestPoint.getX())) {
                theLowestPoint = temporary;
            }
        }
        return theLowestPoint;
    }

    private Set<Point> sortPoints(List<Point> points) {
        if (points.size() == 0) {
            return new TreeSet<>();
        }
        Point theLowestPoint = findTheLowestPointInArea(points);
        TreeSet<Point> set = new TreeSet<>((a, b) -> {
            if (a == b || a.equals(b)) {
                return 0;
            }
            double thetaValueOfA = Math.atan2(a.getY() - theLowestPoint.getY(), a.getX() - theLowestPoint.getX());
            double thetaValueOfB = Math.atan2(b.getY() - theLowestPoint.getY(), b.getX() - theLowestPoint.getX());

            if (thetaValueOfA < thetaValueOfB) {
                return -1;
            } else if (thetaValueOfA > thetaValueOfB) {
                return 1;
            } else {

                double distanceA = Math.sqrt(((theLowestPoint.getX() - a.getX()) * (theLowestPoint.getX() - a.getX())) +
                        ((theLowestPoint.getY() - a.getY()) * (theLowestPoint.getY() - a.getY())));
                double distanceB = Math.sqrt(((theLowestPoint.getX() - b.getX()) * (theLowestPoint.getX() - b.getX())) +
                        ((theLowestPoint.getY() - b.getY()) * (theLowestPoint.getY() - b.getY())));

                if (distanceA < distanceB) {
                    return -1;
                } else {
                    return 1;
                }
            }
        });
        set.addAll(points);
        return set;
    }

    private Turn getTurn(Point a, Point b, Point c) {

        double crossProduct = ((b.getX() - a.getX()) * (c.getY() - a.getY())) - ((b.getY() - a.getY()) * (c.getX() - a.getX()));
        if (crossProduct > 0) {
            return Turn.COUNTER_CLOCKWISE;
        } else if (crossProduct < 0) {
            return Turn.CLOCKWISE;
        } else {
            return Turn.LINEAR;
        }
    }

    private Point getNextToTop(Stack<Point> stack) {
        Point top = stack.pop();
        Point result = stack.peek();
        stack.push(top);
        return result;
    }
}
