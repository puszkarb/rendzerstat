package Voronoi;

public class ContourLine {
    final private Point startingPoint;
    final private Point endingPoint;
    private double aCoefficient;
    private double bCoefficient;
    private boolean isVertical = false;

    public ContourLine(Point startingPoint, Point endingPoint) {
        this.startingPoint = startingPoint;
        this.endingPoint = endingPoint;
        calculateCoefficients();
    }

    private void calculateCoefficients() {
        if (Math.abs(startingPoint.getX() - endingPoint.getX()) < 1) {
            aCoefficient = 0;
            bCoefficient = startingPoint.getX();
            isVertical = true;
        } else {
            aCoefficient = (endingPoint.getY() - startingPoint.getY()) / (endingPoint.getX() - startingPoint.getX());
            bCoefficient = startingPoint.getY() - aCoefficient * startingPoint.getX();
        }
    }

    public Point calculateCommonPoint(Edge diagramEdge) {
        double edgeACoefficient, edgeBCoefficient;
        boolean edgeVertical = false;
        if (Math.abs(diagramEdge.getStartingPoint().getX() - diagramEdge.getEndingPoint().getX()) < 1) {
            edgeACoefficient = 0;
            edgeBCoefficient = diagramEdge.getStartingPoint().getX();
            edgeVertical = true;
        } else {
            edgeACoefficient = (diagramEdge.getEndingPoint().getY() - diagramEdge.getStartingPoint().getY()) / (diagramEdge.getEndingPoint().getX() - diagramEdge.getStartingPoint().getX());
            edgeBCoefficient = diagramEdge.getStartingPoint().getY() - edgeACoefficient * diagramEdge.getStartingPoint().getX();
        }
        double x, y;
        if ((isVertical == edgeVertical && isVertical) || (edgeACoefficient == aCoefficient && !isVertical && !edgeVertical)) {
            return null;
        } else if (isVertical) {
            x = bCoefficient;
            y = edgeACoefficient * x + edgeBCoefficient;
        } else if (edgeVertical) {
            x = edgeBCoefficient;
            y = aCoefficient * x + bCoefficient;
        } else {
            x = (bCoefficient - edgeBCoefficient) / (edgeACoefficient - aCoefficient);
            y = aCoefficient * x + bCoefficient;
        }
        if ((x >= startingPoint.getX() && x <= endingPoint.getX()) || (x >= endingPoint.getX() && x <= startingPoint.getX())) {
            if ((y >= startingPoint.getY() && y <= endingPoint.getY()) || (y >= endingPoint.getY() && y <= startingPoint.getY())) {
                return new Point(x, y);
            }
        }
        return null;
    }

    Point getStartingPoint() {
        return startingPoint;
    }

    Point getEndingPoint() {
        return endingPoint;
    }

    public double calculateYCoordinate(double x) {
        return aCoefficient * x + bCoefficient;
    }
}
