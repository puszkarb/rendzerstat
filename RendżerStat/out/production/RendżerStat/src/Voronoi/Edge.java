package Voronoi;

import java.util.ArrayList;
import java.util.List;

public class Edge extends Node {

    private Point startingPoint;
    private Point endingPoint;
    private double slope;
    private double yCoordinate;
    private Point middlePoint;

    public Edge(Node parent, Node none, Point startingPoint, Point leftFocus, Point rightFocus) {
        super(parent, none);
        this.startingPoint = startingPoint;
        calculateSlopeAndYCoordinate(leftFocus, rightFocus);
    }

    private void calculateSlopeAndYCoordinate(Point leftFocus, Point rightFocus) {
        slope = (rightFocus.getX() - leftFocus.getX()) / (leftFocus.getY() - rightFocus.getY());
        if (Double.isInfinite(slope)) {
            slope = 1000000000.0;
        }
        middlePoint = new Point((leftFocus.getX() + rightFocus.getX()) / 2, (leftFocus.getY() + rightFocus.getY()) / 2);
        yCoordinate = middlePoint.getY() - slope * middlePoint.getX();
    }

    double getSlope() {
        return slope;
    }

    double getyCoordinate() {
        return yCoordinate;
    }

    Point getMiddlePoint() {
        return middlePoint;
    }

    public Point doesIntersectWith(Edge edgeToIntersect) {
        if (slope == edgeToIntersect.getSlope() && yCoordinate == edgeToIntersect.getyCoordinate()) {
            return null;
        }
        double x = (edgeToIntersect.getyCoordinate() - yCoordinate) / (slope - edgeToIntersect.getSlope());
        double y = slope * x + yCoordinate;
        return new Point(x, y);
    }

    public Point getStartingPoint() {
        return startingPoint;
    }

    public void setEndingPoint(Point endingPoint) {
        this.endingPoint = endingPoint;
    }

    public Point getEndingPoint() {
        return endingPoint;
    }

    public void correctWithinBorders(List<ContourLine> contourLines) {
        assert startingPoint != null && endingPoint != null;
        if (checkIfOutOfContour(startingPoint, contourLines)) {
            startingPoint = meetWithContour(contourLines, true);
        }
        if (checkIfOutOfContour(endingPoint, contourLines)) {
            endingPoint = meetWithContour(contourLines, false);
        }
    }

    private boolean checkIfOutOfContour(Point pointToBeChecked, List<ContourLine> contourLines) {
        assert pointToBeChecked != null;
        int numberOfCrossing = getNumberOfCrossing(contourLines, pointToBeChecked.getX(), pointToBeChecked.getY());
        return numberOfCrossing % 2 == 0;
    }

    public static int getNumberOfCrossing(List<ContourLine> contourLines, double x, double y) {
        int numberOfCrossing = 0;
        for (ContourLine cLine : contourLines) {
            if (cLine.getStartingPoint().getX() == x) {
                if ((cLine.getStartingPoint().getY() <= y && cLine.getEndingPoint().getY() > y)
                        || (cLine.getEndingPoint().getY() <= y && cLine.getStartingPoint().getY() > y)) {
                    numberOfCrossing++;
                }
            } else if ((cLine.getStartingPoint().getX() <= x && cLine.getEndingPoint().getX() > x)
                    || (cLine.getEndingPoint().getX() <= x && cLine.getStartingPoint().getX() > x)) {
                double yCommonPoint = cLine.calculateYCoordinate(x);
                if (yCommonPoint >= y) {
                    numberOfCrossing++;
                }
            }
        }
        return numberOfCrossing;
    }

    private Point meetWithContour(List<ContourLine> contourLines, boolean changedStartingPoint) {
        Point oldPoint, newEndingPoint = null;
        List<Point> newEndingPointList = new ArrayList<>();
        if (changedStartingPoint) {
            oldPoint = startingPoint;
        } else {
            oldPoint = endingPoint;
        }
        for (ContourLine line : contourLines) {
            newEndingPoint = line.calculateCommonPoint(this);
            if (newEndingPoint != null) {
                newEndingPointList.add(newEndingPoint);
            }
        }
        double minLength = Double.MAX_VALUE;
        for (Point p : newEndingPointList) {
            double length = Math.sqrt(Math.pow(oldPoint.getX() - p.getX(), 2) + Math.pow(oldPoint.getY() - p.getY(), 2));
            if (length < minLength) {
                minLength = length;
                newEndingPoint = p;
            }
        }
        assert newEndingPoint != null;
        return newEndingPoint;
    }

    @Override
    public double getFirstPointX() {
        return startingPoint.getX();
    }

    @Override
    public double getFirstPointY() {
        return startingPoint.getY();
    }

    @Override
    public boolean isEdge() {
        return true;
    }
}
