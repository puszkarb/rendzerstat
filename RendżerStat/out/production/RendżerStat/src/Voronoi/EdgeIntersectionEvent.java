package Voronoi;

public class EdgeIntersectionEvent implements Event {

    private Arc squeezedArc;
    private Point intersectionPoint;

    public EdgeIntersectionEvent(Point intersectionPoint, Arc squeezedArc) {
        this.intersectionPoint = intersectionPoint;
        this.squeezedArc = squeezedArc;
    }

    public Arc getSqueezedArc() {
        return squeezedArc;
    }

    public Point getIntersectionPoint() {
        return intersectionPoint;
    }

    @Override
    public double getYCoordinatePoint() {
        return intersectionPoint.getY();
    }

    @Override
    public double getXCoordinatePoint() {
        return intersectionPoint.getX();
    }

    @Override
    public Point getPoint() {
        return intersectionPoint;
    }
}
