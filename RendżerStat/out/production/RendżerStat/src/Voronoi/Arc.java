package Voronoi;

public class Arc extends Node {

    final private Point focusPoint;
    private EdgeIntersectionEvent squeezeEvent;

    public Arc(Node parent, Node none, Point focusPoint) {
        super(parent, none);
        this.focusPoint = focusPoint;
    }


    public void setSqueezeEvent(EdgeIntersectionEvent squeezeEvent) {
        this.squeezeEvent = squeezeEvent;
    }

    public EdgeIntersectionEvent getSqueezeEvent() {
        return squeezeEvent;
    }

    public Point getFocusPoint() {
        return focusPoint;
    }

    @Override
    public double getFirstPointX() {
        return focusPoint.getX();
    }

    @Override
    public double getFirstPointY() {
        return focusPoint.getY();
    }

    @Override
    public boolean isEdge() {
        return false;
    }

}
