package Voronoi;

public interface Event {

    double getYCoordinatePoint();

    double getXCoordinatePoint();

    Point getPoint();

}
