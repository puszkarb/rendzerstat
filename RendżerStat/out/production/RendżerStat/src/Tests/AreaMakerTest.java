package Tests;

import ConvexHull.AreaMaker;
import DataReader.Data;
import StatsMaker.Area;
import StatsMaker.StatObject;
import StatsMaker.StatsMaker;
import Voronoi.ContourLine;
import Voronoi.Point;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class AreaMakerTest {

    private AreaMaker areaMaker;
    private StatsMaker statsMaker;
    private Data data;
    private List<ContourLine> contourLines;

    @Before
    public void setUp() {
        statsMaker = new StatsMaker();
        data = new Data();
        areaMaker = new AreaMaker(data, statsMaker);
        contourLines = new ArrayList<>();
    }

    @Test
    public void createNewAreas() {
        //Given
        StatObject keyPoint1 = new StatObject(true, false, false, "keyPoint1");
        keyPoint1.setX(100);
        keyPoint1.setY(300);

        StatObject keyPoint2 = new StatObject(true, false, false, "keyPoint2");
        keyPoint2.setX(200);
        keyPoint2.setY(200);

        StatObject keyPoint3 = new StatObject(true, false, false, "keyPoint3");
        keyPoint3.setX(300);
        keyPoint3.setY(400);

        StatObject keyPoint4 = new StatObject(true, false, false, "keyPoint4");
        keyPoint4.setX(300);
        keyPoint4.setY(100);

        StatObject keyPoint5 = new StatObject(true, false, false, "keyPoint5");
        keyPoint5.setX(150);
        keyPoint5.setY(110);

        data.addKeyObject(keyPoint1);
        data.addKeyObject(keyPoint2);
        data.addKeyObject(keyPoint3);
        data.addKeyObject(keyPoint4);
        data.addKeyObject(keyPoint5);
        setContours();

        data.refreshKeyObjectList();
        statsMaker.addNewKeyPoint(data.getKeyObjects());
        statsMaker.prepareDiagram(contourLines);

        //When
        areaMaker.createNewAreas();

        //Then
        for (Point p : areaMaker.pointsArrayList) {
            System.out.println("(" + p.getX() + ", " + p.getY() + ")");
        }
        for (Area a : areaMaker.getAreasList()) {
            a.printAreaDescription();
        }
        assertEquals(5, areaMaker.getAreasList().size());
    }

    private void setContours() {
        ContourLine contour = new ContourLine(new Point(0, 0), new Point(1000, 0));
        contourLines.add(contour);
        contour = new ContourLine(new Point(1000, 0), new Point(1000, 1000));
        contourLines.add(contour);
        contour = new ContourLine(new Point(1000, 1000), new Point(0, 1000));
        contourLines.add(contour);
        contour = new ContourLine(new Point(0, 1000), new Point(0, 0));
        contourLines.add(contour);
    }

}
