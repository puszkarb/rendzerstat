package Tests;

import Voronoi.ContourLine;
import Voronoi.Edge;
import Voronoi.Point;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


public class EdgeTest {

    private List<ContourLine> contourLines;

    @Before
    public void setUp() {
        contourLines = new ArrayList<>();
        setContours();
    }

    @Test
    public void intersectWith() {
        //GIVEN
        Edge firstEdge = new Edge(null, null, new Point(34, 56), new Point(15, 30), new Point(45, 45));
        Edge secondEdge = new Edge(null, null, new Point(67, 66), new Point(45, 30), new Point(87, 60));

        //WHEN
        Point intersection = secondEdge.doesIntersectWith(firstEdge);

        //THEN
        assertNotNull(intersection);
        assertEquals(-66.49999999999996, intersection.getX(), 0);
        assertEquals(230.49999999999991, intersection.getY(), 0);
    }

    @Test
    public void notIntersectWith() {
        //GIVEN
        Edge firstEdge = new Edge(null, null, new Point(34, 76), new Point(15, 50), new Point(45, 66));
        Edge secondEdge = new Edge(null, null, new Point(54, 59), new Point(15, 50), new Point(45, 66));

        //WHEN
        Point intersection = secondEdge.doesIntersectWith(firstEdge);

        //THEN
        assertNull(intersection);
    }

    @Test
    public void calculateWithinBordersTest() {
        //GIVEN
        Edge incorrectXEdge = new Edge(null, null, new Point(-3, 12), new Point(1, 1), new Point(1, 1));
        incorrectXEdge.setEndingPoint(new Point(15, 5));
        Edge incorrectYEdge = new Edge(null, null, new Point(11, -2), new Point(1, 1), new Point(1, 1));
        incorrectYEdge.setEndingPoint(new Point(2, 16));
        Edge theSameY = new Edge(null, null, new Point(-3, 3), new Point(1, 1), new Point(1, 1));
        theSameY.setEndingPoint(new Point(16, 3));
        Edge theSameX = new Edge(null, null, new Point(4, -3), new Point(1, 1), new Point(1, 1));
        theSameX.setEndingPoint(new Point(4, 16));

        //WHEN
        incorrectXEdge.correctWithinBorders(contourLines);
        incorrectYEdge.correctWithinBorders(contourLines);
        theSameX.correctWithinBorders(contourLines);
        theSameY.correctWithinBorders(contourLines);

        //THEN
        checkEdge(incorrectXEdge, new double[]{0, 10.833333333333334, 14, 5.388888888888888});
        checkEdge(incorrectYEdge, new double[]{10, 0, 3, 14});
        checkEdge(theSameX, new double[]{4, 0, 4, 14});
        checkEdge(theSameY, new double[]{0, 3, 14, 3});
    }

    @Test
    public void calculateWhenCoordinatesAreOutsideBorder() {
        //GIVEN
        Edge leftUpRightDownEdge = new Edge(null, null, new Point(-2, -2), new Point(1, 1), new Point(1, 1));
        leftUpRightDownEdge.setEndingPoint(new Point(15, 15));
        Edge rightUpLeftDownEdge = new Edge(null, null, new Point(15, -1), new Point(1, 1), new Point(1, 1));
        rightUpLeftDownEdge.setEndingPoint(new Point(-1, 15));

        //WHEN
        leftUpRightDownEdge.correctWithinBorders(contourLines);
        rightUpLeftDownEdge.correctWithinBorders(contourLines);

        //THEN
        checkEdge(leftUpRightDownEdge, new double[]{0, 0, 14, 14});
        checkEdge(rightUpLeftDownEdge, new double[]{14, 0, 0, 14});
    }

    @Test
    public void changeCoordinates() {
        //GIVEN
        Edge rightUp = new Edge(null, null, new Point(15, -3), new Point(1, 1), new Point(1, 1));
        rightUp.setEndingPoint(new Point(9, 6));
        Edge leftDown = new Edge(null, null, new Point(-2, 15), new Point(1, 1), new Point(1, 1));
        leftDown.setEndingPoint(new Point(3, 5));

        //WHEN
        rightUp.correctWithinBorders(contourLines);
        leftDown.correctWithinBorders(contourLines);

        //THEN
        checkEdge(rightUp, new double[]{13, 0, 9, 6});
        checkEdge(leftDown, new double[]{0, 11, 3, 5});
    }

    @Test
    public void getNumberOfCrossingTest() {
        //GIVEN
        Point innerPoint = new Point(12, 8);
        Point outerPoint = new Point(54, 98);
        Point verticalOuter = new Point(14, -9);
        Point verticalInner = new Point(14, 5);
        Point horizontalOuter = new Point(65, 0);

        //WHEN
        checkPoint(innerPoint, true);
        checkPoint(outerPoint, false);
        checkPoint(verticalInner, true);
        checkPoint(verticalOuter, false);
        checkPoint(horizontalOuter, false);
    }

    private void checkPoint(Point point, boolean isInner) {
        int numberOfCrossing = Edge.getNumberOfCrossing(contourLines, point.getX(), point.getY());
        if (isInner) {
            assertEquals(1, numberOfCrossing);
        } else {
            assertEquals(0, numberOfCrossing);
        }
    }

    private void checkEdge(Edge checkedEdge, double[] coordinates) {
        assertEquals(coordinates[0], checkedEdge.getStartingPoint().getX(), 0);
        assertEquals(coordinates[1], checkedEdge.getStartingPoint().getY(), 0);
        assertEquals(coordinates[2], checkedEdge.getEndingPoint().getX(), 0);
        assertEquals(coordinates[3], checkedEdge.getEndingPoint().getY(), 0);
    }

    private void setContours() {
        ContourLine contour = new ContourLine(new Point(0, 0), new Point(14, 0));
        contourLines.add(contour);
        contour = new ContourLine(new Point(14, 0), new Point(14, 14));
        contourLines.add(contour);
        contour = new ContourLine(new Point(14, 14), new Point(0, 14));
        contourLines.add(contour);
        contour = new ContourLine(new Point(0, 14), new Point(0, 0));
        contourLines.add(contour);
    }
}