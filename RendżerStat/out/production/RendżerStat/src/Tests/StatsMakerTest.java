package Tests;

import StatsMaker.StatsMaker;
import Voronoi.ContourLine;
import Voronoi.Edge;
import Voronoi.Point;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class StatsMakerTest {

    private List<Point> keyPoints;
    private List<ContourLine> contourLines;
    private StatsMaker shakerShaker;

    @Before
    public void setUp() {
        contourLines = new ArrayList<>();
        setContours();
        keyPoints = new ArrayList<>();
        keyPoints.add(new Point(357, 340));
        keyPoints.add(new Point(579, 377));
        keyPoints.add(new Point(501, 534));
        keyPoints.add(new Point(331, 555));
        keyPoints.add(new Point(471, 300));
        keyPoints.add(new Point(732, 507));
        keyPoints.add(new Point(305, 470));
        shakerShaker = new StatsMaker(keyPoints);
    }

    @Test
    public void prepareDiagram() {
        //GIVEN
        List<Edge> endedEdges;

        //WHEN
        shakerShaker.prepareDiagram(contourLines);

        //THEN
        endedEdges = shakerShaker.getFinishedEdges();
        assertEquals(18, endedEdges.size());
        assertEquals(351.96538461538444, endedEdges.get(9).getStartingPoint().getY(), 0);
        assertEquals(1500, endedEdges.get(10).getEndingPoint().getY(), 0);
        assertEquals(0, endedEdges.get(15).getEndingPoint().getX(), 0);
        assertEquals(0, endedEdges.get(17).getEndingPoint().getY(), 0);
    }

    @Test
    public void checkTheSameY() {
        //GIVEN
        keyPoints.clear();
        keyPoints.add(new Point(106, 100));
        keyPoints.add(new Point(100, 100));
        shakerShaker = new StatsMaker(keyPoints);

        //WHEN
        shakerShaker.prepareDiagram(contourLines);

        //THEN
        List<Edge> endedEdges = shakerShaker.getFinishedEdges();
        assertEquals(1, endedEdges.size());
        Edge endedEdge = endedEdges.get(0);
        assertEquals(103, endedEdge.getStartingPoint().getX(), 0);
        assertEquals(0, endedEdge.getStartingPoint().getY(), 0);
        assertEquals(103, endedEdge.getEndingPoint().getX(), 0);
        assertEquals(1500, endedEdge.getEndingPoint().getY(), 0);
    }

    @Test
    public void checkTheSameX() {
        //GIVEN
        keyPoints.clear();
        keyPoints.add(new Point(400, 150));
        keyPoints.add(new Point(400, 300));
        shakerShaker = new StatsMaker(keyPoints);

        //WHEN
        shakerShaker.prepareDiagram(contourLines);

        //THEN
        List<Edge> endedEdges = shakerShaker.getFinishedEdges();
        assertEquals(2, endedEdges.size());
    }

    private void setContours() {
        ContourLine contour = new ContourLine(new Point(0, 0), new Point(1500, 0));
        contourLines.add(contour);
        contour = new ContourLine(new Point(1500, 0), new Point(1500, 1500));
        contourLines.add(contour);
        contour = new ContourLine(new Point(1500, 1500), new Point(0, 1500));
        contourLines.add(contour);
        contour = new ContourLine(new Point(0, 1500), new Point(0, 0));
        contourLines.add(contour);
    }
}