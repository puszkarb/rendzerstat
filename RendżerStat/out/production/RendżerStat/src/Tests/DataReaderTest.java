package Tests;

import DataReader.Data;
import StatsMaker.StatObject;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class DataReaderTest {

    @Test
    public void readFileTest() {
        //GIVEN
        File file = new File("correctFile.txt");
        Data reader = new Data();
        List<StatObject> keyObjects, contours, objects, objectsDefinitions;

        //WHEN
        assertEquals(0, reader.readFile(file, false));

        //THEN
        keyObjects = reader.getKeyObjects();
        contours = reader.getContours();
        objects = reader.getObjects();
        objectsDefinitions = reader.getObjectsDefinitions();
        assertEquals(5, contours.size());
        assertEquals(3, keyObjects.size());
        assertEquals(6, objectsDefinitions.size());
        assertEquals(11, objects.size());
    }

    @Test
    public void notExistingFileTest() {
        //GIVEN
        File notExistingFile = new File("nieMaTakiegoNumeru.txt");
        Data reader = new Data();

        //WHEN
        assertEquals(666, reader.readFile(notExistingFile, false));
    }

    @Test
    public void badCommentsInFile() {
        //GIVEN
        File incorrectCommentFile = new File("incorrectComment.txt");
        Data reader = new Data();

        //WHEN
        assertEquals(997, reader.readFile(incorrectCommentFile, false));
    }

    @Test
    public void failureInObjectDefinition() {
        //GIVEN
        File incorrectObjectDefinitionFile = new File("incorrectObjectDefinition.txt");
        Data reader = new Data();

        //WHEN
        assertEquals(3, reader.readFile(incorrectObjectDefinitionFile, false));
    }

    @Test
    public void failureInBorderDefinition() {
        //GIVEN
        File incorrectContoursFile = new File("incorrectContours.txt");
        Data reader = new Data();

        //THEN
        assertEquals(2, reader.readFile(incorrectContoursFile, false));
    }

    @Test
    public void failureInKeyPointsDefinition() {
        //GIVEN
        File incorrectKeyPointsFile = new File("incorrectKeyPoints.txt");
        Data reader = new Data();

        //WHEN
        assertEquals(888, reader.readFile(incorrectKeyPointsFile, false));
    }

    @Test
    public void failureInObjectsSection() {
        //GIVEN
        File incorrectObjectsFile = new File("incorrectObjects.txt");
        Data reader = new Data();

        //WHEN
        assertEquals(7, reader.readFile(incorrectObjectsFile, false));
    }
}