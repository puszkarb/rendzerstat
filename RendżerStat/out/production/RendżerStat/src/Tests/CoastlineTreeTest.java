package Tests;

import Voronoi.*;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.*;

public class CoastlineTreeTest {

    private CoastlineTree testTree;
    private PriorityQueue<Event> events;

    @Before
    public void setUp() {
        testTree = new CoastlineTree(new KeyPointEvent(new Point(10, 10)));
        events = new PriorityQueue<>(15, new EventComparator());
    }

    private void checkFiveNodes(Node nodeToStartWith, Point oldArcPoint, Point newArcPoint, Point newNewPoint) {
        assertThat(nodeToStartWith, instanceOf(Edge.class));
        assertEquals(newArcPoint.getX(), nodeToStartWith.getFirstPointX(), 0);
        Node leftChild = nodeToStartWith.getLeftChild(), rightChild = nodeToStartWith.getRightChild();
        assertThat(leftChild, instanceOf(Arc.class));
        assertThat(rightChild, instanceOf(Edge.class));
        assertEquals(oldArcPoint.getX(), leftChild.getFirstPointX(), 0);
        assertEquals(newArcPoint.getX(), rightChild.getFirstPointX(), 0);
        if (newNewPoint == null) {
            assertEquals(newArcPoint.getX(), rightChild.getLeftChild().getFirstPointX(), 0);
        } else {
            assertEquals(newNewPoint.getX(), rightChild.getLeftChild().getFirstPointX(), 0);
        }
        assertEquals(oldArcPoint.getX(), rightChild.getRightChild().getFirstPointX(), 0);
    }

    @Test
    public void addArcTest() {
        //GIVEN
        List<Node> nodes;
        KeyPointEvent event = new KeyPointEvent(new Point(15.0, 17.0));

        //WHEN
        testTree.addArc(event, events);
        testTree.printTreeInOrder(testTree.getRoot());
        nodes = testTree.getNodes();

        //THEN
        Node treeRoot = testTree.getRoot();
        assertEquals(5, nodes.size(), 0);
        checkFiveNodes(treeRoot, new Point(10, 10), new Point(15, 17), null);
    }

    @Test
    public void addTwoArcsTest() {
        //GIVEN
        List<Node> nodes;
        KeyPointEvent eventPoint1 = new KeyPointEvent(new Point(15.0, 17.0));
        testTree.addArc(eventPoint1, events);
        KeyPointEvent eventPoint2 = new KeyPointEvent(new Point(14.0, 20.0));

        //WHEN
        testTree.addArc(eventPoint2, events);
        testTree.printTreeInOrder(testTree.getRoot());
        nodes = testTree.getNodes();

        //THEN
        assertEquals(9, nodes.size(), 0);
        checkFiveNodes(testTree.getRoot(), new Point(10, 10), new Point(15, 17), new Point(14, 20));
        Node startNode = testTree.getRoot().getRightChild().getLeftChild();
        checkFiveNodes(startNode, new Point(15, 17), new Point(14, 20), null);
        assertEquals(1, events.size());
        EdgeIntersectionEvent event = (EdgeIntersectionEvent) events.peek();
        assertNotNull(event);
        assertEquals(23.113287300810907, event.getYCoordinatePoint(), 0);
    }

    @Test
    public void endEdgesTest() {
        //GIVEN
        KeyPointEvent eventPoint1 = new KeyPointEvent(new Point(15.1, 17.0));
        testTree.addArc(eventPoint1, events);
        KeyPointEvent eventPoint2 = new KeyPointEvent(new Point(14.0, 20.0));
        testTree.addArc(eventPoint2, events);
        List<Edge> endedEdges = new ArrayList<>();
        testTree.deleteArc(events, (EdgeIntersectionEvent) events.remove(), endedEdges);

        //WHEN
        assertTrue(events.isEmpty());
        testTree.endEdges(testTree.getRoot(), endedEdges);

        //THEN
        assertEquals(5, endedEdges.size());
        for (Edge e : endedEdges) {
            assertNotNull(e.getEndingPoint());
        }
    }

    @Test
    public void deleteArcTest() {
        //GIVEN
        KeyPointEvent keyPointEvent1 = new KeyPointEvent(new Point(15.0, 17.0));
        KeyPointEvent keyPointEvent2 = new KeyPointEvent(new Point(14.0, 20.0));
        testTree.addArc(keyPointEvent1, events);
        testTree.addArc(keyPointEvent2, events);
        EdgeIntersectionEvent intersectionEvent = (EdgeIntersectionEvent) events.remove();
        List<Edge> endedEdges = new ArrayList<>();

        //WHEN
        testTree.deleteArc(events, intersectionEvent, endedEdges);
        testTree.printTreeInOrder(testTree.getRoot());
        List<Node> nodes = testTree.getNodes();

        //THEN
        Node treeRoot = testTree.getRoot();
        assertEquals(7, nodes.size());
        checkSevenNodes(treeRoot, new Point(10, 10), new Point(15, 17), new Point(14, 20));
    }

    private void checkSevenNodes(Node root, Point rootPoint, Point firstAddedPoint, Point secondAddedPoint) {
        assertThat(root, instanceOf(Edge.class));
        Node rightChild = root.getRightChild();
        assertNotNull(rightChild);
        assertEquals(rootPoint.getX(), root.getLeftChild().getFirstPointX(), 0);
        assertEquals(firstAddedPoint.getX(), rightChild.getFirstPointX(), 0);
        assertEquals(secondAddedPoint.getX(), rightChild.getLeftChild().getFirstPointX(), 0);
        assertEquals(rootPoint.getX(), rightChild.getRightChild().getFirstPointX(), 0);
        assertEquals(secondAddedPoint.getX(), rightChild.getLeftChild().getLeftChild().getFirstPointX(), 0);
        assertEquals(firstAddedPoint.getX(), rightChild.getLeftChild().getRightChild().getFirstPointX(), 0);
    }

}