package GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class RendżerStat extends Application {

    public Controller editorStageController;
    public Controller mainStageController;
    private Stage primaryStage;


    public static void main(String[] args) {
        launch(args);
    }


    @Override
    public void start(final Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        mainStageController = load("gui_koncept.fxml");
        editorStageController = load("gui_koncept_edycja.fxml");
        this.primaryStage.setTitle("RendżerStat");
        mainStageController.toMainStage();
        primaryStage.show();

    }

    public Controller load(final String s) throws IOException {
        final FXMLLoader loader = new FXMLLoader(getClass().getResource(s));
        loader.load();
        final Controller controller = loader.getController();
        controller.configure(this, new Scene(loader.getRoot()));
        return controller;
    }

    public void show(final Scene scene) {
        primaryStage.setScene(scene);
    }
}
