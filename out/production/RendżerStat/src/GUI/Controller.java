package GUI;

import DataReader.DataReader;
import com.jfoenix.controls.JFXToggleButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

public class Controller {

    private RendżerStat application;
    private Scene scene;

    @FXML
    private JFXToggleButton mainToEditorButton;

    @FXML
    private JFXToggleButton editorToMainButton;


    public void configure(final RendżerStat application, final Scene scene) {
        this.application = application;
        this.scene = scene;
    }

    public void show() {
        application.show(scene);
    }

    public void toMainStage() {
        application.mainStageController.show();
    }


    public void switchToEditionStage(ActionEvent actionEvent) {
        application.editorStageController.setEditorToggleButtonActive(true);
        application.editorStageController.show();
    }

    public void switchToMainStage(ActionEvent actionEvent) {
        application.mainStageController.setMainToggleButtonActive(false);
        application.mainStageController.show();
    }

    public void setEditorToggleButtonActive(boolean toggleButtonActive) {
        this.editorToMainButton.setSelected(toggleButtonActive);
    }

    public void setMainToggleButtonActive(boolean toggleButtonActive) {
        this.mainToEditorButton.setSelected(toggleButtonActive);
    }

    public void showOpenFileChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Otwórz Plik");
     //   fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("*.txt", ".txt"));
        File file = fileChooser.showOpenDialog(new Stage());
        if (file != null) {
            System.out.println("DUPA ----> otwieram");
            //otwieranie plików --- komentarz tez do wywalenia
            loadData(file);
        }
    }

    public void showSaveFileChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Otwórz Plik");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("*.txt", ".txt"));
        File file = fileChooser.showSaveDialog(new Stage());
        if (file != null) {
            System.out.println("DUPA -----> zapisuje");
            //saving methods --- komentarz tez do wywalenia

        }
    }

    private void loadData(File file) {
        DataReader dataReader = new DataReader(file);
        try {
            dataReader.readFile();
        } catch (IOException e) {
            System.out.println("Jebło w ładowaniu pliku - komunikat z controllera: \n" + e.getMessage());
        }
    }
}
