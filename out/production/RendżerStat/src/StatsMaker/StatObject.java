package StatsMaker;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class StatObject {
    private boolean isKeyObject;
    private String objectName;
    private List<Pair <String, Object>> properties;
    private int regionId;
    private int x;
    private int y;

    public StatObject(boolean isKeyObject, String objectName){
        this.isKeyObject = isKeyObject;
        this.objectName = objectName;
        properties = new ArrayList<>();
    }

    public void addProperty(String label, Object value){
        properties.add(new Pair<>(label, value));
    }

}